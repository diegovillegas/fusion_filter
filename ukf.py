#!/usr/bin/env python
import numpy as np
import rosbag
from constants import *
from ukf_functions import *
from ukf_classes import filter_state, sensor
import matplotlib.pyplot as plt
from scipy.linalg import cholesky

from filterpy.kalman import MerweScaledSigmaPoints as SigmaPoints
from filterpy.kalman import UnscentedKalmanFilter as UKF

# sensors
''' GPS SENSOR has the form:  [X, Y, BETA_Z]'''
GPS = sensor( H_gps,        dim_x=5, dim_z=4 )
''' ENC SENSOR has the form:  [STEERING, REAR_ENCODER]'''
ENC = sensor( H_steer_renc, dim_x=5, dim_z=2 )
''' IMU SENSOR has the form:  [BETA_Z] '''
IMU = sensor( H_imu,        dim_x=5, dim_z=1 )

# fusion filter struct
ukf = filter_state( dim_x=5, dim_z=2, dt=0.05, fx=F_, sqrt_fn = cholesky,
                   points_parameters = [0.9, 2.0, 0])#,
                   # x_mean_fn=state_mean,
                   #residual_x=residual_x,
                   #residual_z=residual_h ) #[0.35, 2.1, -2.3]

# to store outputs
GPS_x_gains, ENC_x_gains, IMU_x_gains = [], [], []
GPS_signals, ENC_signals, IMU_signals = [], [], []
GPS_signals_e, ENC_signals_e, IMU_signals_e = [], [], []
ENC_outputs, dts_ukf = [], []
UKF_states, UKF_states_e = [], []
auxiliar = []
UKF_incr = []
times_ukf, times_gps, times_enc, times_imu = [], [], [], []
# files
bagfile_list = ['velo_test_1_2016-09-21-13-57-02_0.bag',
'velo_test_1_2016-09-21-13-58-02_1.bag',
'velo_test_1_2016-09-21-13-59-02_2.bag',
'velo_test_1_2016-09-21-14-00-02_3.bag',
'velo_test_1_2016-09-21-14-01-02_4.bag',
'velo_test_1_2016-09-21-14-02-02_5.bag',
'velo_test_1_2016-09-21-14-03-02_6.bag',
'velo_test_1_2016-09-21-14-04-02_7.bag',
'velo_test_1_2016-09-21-14-05-02_8.bag',
'velo_test_1_2016-09-21-14-06-02_9.bag',
'velo_test_1_2016-09-21-14-07-02_10.bag',
'velo_test_1_2016-09-21-14-08-02_11.bag',
'velo_test_1_2016-09-21-14-09-02_12.bag',
'velo_test_1_2016-09-21-14-10-02_13.bag',
'velo_test_1_2016-09-21-14-11-02_14.bag',
'velo_test_1_2016-09-21-14-12-02_15.bag',
'velo_test_1_2016-09-21-14-13-02_16.bag',
'velo_test_1_2016-09-21-14-14-02_17.bag']


path_bagfile = '/media/diego/DataPart2/UC3M/2doCuatrimestre/Trabajo_LSI/data_icab/'
mapbag_07 = 'map_test_2016-06-13-13-03-45_7.bag'
mapbag_00 = 'map_test_2016-06-13-12-49-45_0.bag'
mapbag_01 = 'map_test_2016-06-13-12-51-45_1.bag'
mapbag_02 = 'map_test_2016-06-13-12-53-45_2.bag'

for bagfile in bagfile_list[2:3]:
    print 'Opening filebags...', bagfile
    #bagfile = mapbag_02
    bag = rosbag.Bag((path_bagfile + bagfile))

    # rostopics of interest
    if bagfile.startswith('velo'):
        topic_POS2 = '/icab1/mavros/global_position/global' # type: nav_msgs/Odometry
        topic_POS = '/icab1/mavros/global_position/local' # type: nav_msgs/Odometry
        topic_ENC = '/icab1/movement_manager/status_info' # type: icab_msgs/StatusInfo
        topic_IMU = '/icab1/mavros/imu/data'              # type: sensor_msgs/Imu
        topic_MAG = '/icab1/mavros/imu/mag'
    else:
        topic_POS2 ='/mavros/global_position/global' # type: nav_msgs/Odometry
        topic_POS = '/mavros/global_position/local' # type: nav_msgs/Odometry
        topic_ENC = '/movement_manager/status_info' # type: icab_msgs/StatusInfo
        topic_IMU = '/mavros/imu/data'              # type: sensor_msgs/Imu
        topic_MAG = '/mavros/imu/mag'
        
    latitudes, longitudes = [], []
    # sensor fusion sequencer
    for topic_, msg_, t_ in bag.read_messages():
        #if topic_ == topic_MAG:
            #print msg_
            #break
        #if (topic_ == topic_POS2):
        #    latitudes.append(msg_.latitude)
        #    longitudes.append(msg_.longitude)
        #===================================================#
        #                      READING GPS                  # 520ms
        #===================================================#
        if topic_ == topic_POS:
            time_gps = t_.to_sec()
            __, __, init_orientation = quat2angle(msg_.pose.pose.orientation)
            #if GPS._samples==0:
            #    GPS.offset = np.array([msg_.pose.pose.position.x.real, msg_.pose.pose.position.y.real, 0, 0])
            GPS_signals.append(GPS.value)
            times_gps.append(time_gps)
            x, y = msg_.pose.pose.position.x.real, msg_.pose.pose.position.y.real
            velx, vely = msg_.twist.twist.linear.x, msg_.twist.twist.linear.y
            z = np.array([x, y, velx, vely])
            R = np.diag((msg_.pose.covariance[0].real, msg_.pose.covariance[7].real,
                        msg_.twist.covariance[0], msg_.twist.covariance[1]))

            GPS.preValue = GPS.value
            GPS.preTime = GPS.time
            GPS.update_data( z, R, time_gps )
            #if GPS._samples == 0:
            #    GPS.offset = GPS.value
            GPS._samples += 1
            GPS._meas_flg = True
            GPS.time = time_gps
        else:
            GPS._meas_flg = False
        #===================================================#
        #                      READING ENCODER              # 50ms
        #===================================================#
        if topic_ == topic_ENC :
            time_enc = t_.to_sec()
            ENC.th = 0.2107# 0.2107 # al 90
            if ENC._samples == 0:
                dt_enc = 0.05
                ENC.ltime = time_enc
                ENC.offset = np.array([ (msg_.dir.angle.real), msg_.trc.val_encoder.real])
                ENC.value = np.array([ (msg_.dir.angle.real), msg_.trc.val_encoder.real])
                ENC.prev_value = ENC.value
                ENC._samples += 1
            else:
                dt_enc = time_enc-ENC.ltime
                data = np.array([msg_.dir.angle.real-ENC.prev_value[0], msg_.trc.val_encoder.real-ENC.prev_value[1]])
                R    = np.diag(( v_steer**2, v_rear_encoder**2 ))
                ENC.prev_value = ENC.value
                ENC.prev_time = ENC.ltime
                ENC.update_data( data, R, time_enc )
                ENC._meas_flg = True
                ENC._samples += 1
                IMU.ltime = time_enc
                times_enc.append(time_enc)
                ENC_signals.append(ENC.value)
                ENC_signals_e.append(ENC.R.diagonal())
        else:
            ENC._meas_flg = False
        #===================================================#
        #                      READING IMU                  # 180ms
        #===================================================#
        if topic_ == topic_IMU and GPS._samples>0:
            time_imu = t_.to_sec()
            IMU.th = 0.0158 #0.0158 # al 90/100 de confianza #0.4549 #al 50
            b_x, b_y, b_z = quat2angle( msg_.orientation )
            #print msg_.angular_velocity.z
            if IMU._samples == 0:
                IMU.value = np.array([GPS.value[2]+np.radians(msg_.angular_velocity.z*dt)])
                ORIENTA_INIT = GPS.value[2]
                IMU.prev_value = IMU.value
                IMU.ltime = time_imu
                IMU.prev_time = IMU.ltime
                IMU._samples += 1
            else:
                dt_imu = time_imu - IMU.ltime
                IMU.R = np.array([msg_.angular_velocity_covariance[8]*dt_imu])
                IMU.value = np.array([msg_.angular_velocity.z*dt_imu])
                IMU.prev_value = IMU.value
                IMU.prev_time  = IMU.ltime
                IMU._meas_flg  = True
                IMU._samples += 1
                IMU.ltime = time_imu
                IMU.rawdata.append([dt_imu, msg_.angular_velocity.x, msg_.angular_velocity.y, msg_.angular_velocity.z, msg_.linear_acceleration.x, msg_.linear_acceleration.y, msg_.linear_acceleration.z])
                IMU_signals.append(IMU.value)
                IMU_signals_e.append(IMU.R[0])
                times_imu.append(time_imu)
        else:
            IMU._meas_flg= False
        #=================================#
        #  initializing the system state...
        #=================================#
        if GPS._samples>5 and ENC._samples>5 and IMU._samples>5 and not ukf.init_flag :
            ukf.x = np.array([ GPS.value[0], GPS.value[1], GPS.value[2], GPS.value[3], ENC.value[0]])
            ukf.init_flag = True
            print 'initialized fusion filter structure .........'
            pos = [GPS.value[0], GPS.value[1]]
        #================================================#
        # COMPUTE  dt ...................................#
        #================================================#
        if GPS._meas_flg and ukf.init_flag :
            #print 'LAST TIME:', GPS.ltime, 'PREV TIEM:', GPS.prev_time
            dt = GPS.time - GPS.preTime
            ukf.predict(dt)
            #================================================#
            #      PREDICTION    STEP                        #
            #================================================#
            # computing the innovation for each sensor
            # ENC --------------------------------------------
            ( ENC.x_gain, ENC.P_gain )  = ukf.calc_param( ENC, type_=2, fx_args=(pos) )
            #ENC.x_gain[2] = normalize_angle(ENC.x_gain[2])
            #ENC.x_gain[4] = normalize_steering(ENC.x_gain[4])
            # IMU --------------------------------------------
            ( IMU.x_gain, IMU.P_gain )  = ukf.calc_param( IMU, type_=1, fx_args=() )
            #IMU.x_gain[2] = normalize_angle(IMU.x_gain[2])
            #IMU.x_gain[4] = normalize_steering(IMU.x_gain[4])
            # GPS --------------------------------------------
            ( GPS.x_gain, GPS.P_gain ) = ukf.calc_param( GPS, type_=0, fx_args=() )
            #GPS.x_gain[2] = normalize_angle(GPS.x_gain[2])
            #GPS.x_gain[4] = normalize_steering(GPS.x_gain[4])
            #=================================================#
            #        UPDATE     STEP                          #
            #=================================================#
            if ENC.q > ENC.th :
                ENC.status = False
                if ENC._samples % 100 ==0:
                    print 'ENC SIGNAL FLAWS'
            else: ENC.status = True
            if IMU.q > IMU.th :
                IMU.status = False
                if IMU._samples % 100 ==0:
                    print 'IMU SIGNAL FLAWS'
            else: IMU.status = True
            ukf.update_state( [GPS.x_gain*GPS.status, ENC.x_gain*ENC.status, IMU.x_gain*IMU.status],
                              [GPS.P_gain*GPS.status, ENC.P_gain*ENC.status, IMU.P_gain*IMU.status] )
            #ukf.x[2] = normalize_angle(ukf.x[2])
            GPS.preTime = GPS.time
            GPS.preValue = GPS.value
            prev_x = ukf.x[0]
            # saving results and measures
            # Save all x_gain and signals from gps, enc and imu sensors
            

            UKF_states.append(ukf.x)
            UKF_states_e.append(ukf.P.diagonal())
            UKF_incr.append((ukf.x-ukf.prev_x))
            times_ukf.append(time_gps)
            dts_ukf.append(time_gps-times_ukf[0])
            GPS_x_gains.append(GPS.x_gain)
            GPS_signals_e.append(GPS.R.diagonal())
            ENC_x_gains.append(ENC.x_gain)
            IMU_x_gains.append(IMU.x_gain)

GPSgains = np.array(GPS_x_gains)
ENCgains = np.array(ENC_x_gains)
IMUgains = np.array(IMU_x_gains)
ENC_outputs_array = []
GPS_signals_array = np.array(GPS_signals)
GPS_signals_e_array = np.array(GPS_signals_e)
ENC_signals_array = np.array(ENC_signals)
ENC_signals_e_array = np.array(ENC_signals_e)
IMU_RAW = np.array(IMU.rawdata)
UKF_states_array = np.array(UKF_states)
UKF_states_e_array = np.array(UKF_states_e)

#SENSOR GAINS#
if False:
    plt.plot( dts_ukf,         GPSgains[:,4], 'g-+', label='GPS', lw = 1)   
    plt.plot( dts_ukf,         ENCgains[:,4], 'k-+', label='ENC', lw = 1)   
    plt.plot( dts_ukf,         IMUgains[:,4], 'b-+', label='IMU', lw = 1)   
    plt.plot( dts_ukf, UKF_states_array[:,4], 'r-o', label='UKF', lw = 1)

    plt.plot( dts_ukf,         GPSgains[:,3], 'g-+', label='GPS', lw = 1)   
    plt.plot( dts_ukf,         ENCgains[:,3], 'k-+', label='ENC', lw = 1)   
    plt.plot( dts_ukf,         IMUgains[:,3], 'b-+', label='IMU', lw = 1)   
    plt.plot( dts_ukf, UKF_states_array[:,3], 'r-o', label='UKF', lw = 1)

    plt.plot( dts_ukf,         GPSgains[:,2], 'g-+', label='GPS', lw = 1)   
    #plt.plot( dts_ukf,         ENCgains[:,2], 'k-+', label='ENC', lw = 1)
    plt.plot( dts_ukf,         IMUgains[:,2], 'b-+', label='IMU', lw = 1)
    plt.plot( dts_ukf, UKF_states_array[:,2], 'r-o', label='UKF', lw = 1)

    plt.plot( dts_ukf,         GPSgains[:,1], 'g-+', label='GPS', lw = 1)
    plt.plot( dts_ukf,         ENCgains[:,1], 'k-+', label='ENC', lw = 1)
    plt.plot( dts_ukf,         IMUgains[:,1], 'b-+', label='IMU', lw = 1)
    plt.plot( dts_ukf, UKF_states_array[:,1], 'r-o', label='UKF', lw = 1)

    plt.plot( dts_ukf,         GPSgains[:,0], 'g-+', label='GPS', lw = 1)
    plt.plot( dts_ukf,         ENCgains[:,0], 'k-+', label='ENC', lw = 1)
    plt.plot( dts_ukf,         IMUgains[:,0], 'b-+', label='IMU', lw = 1)
    plt.plot( dts_ukf, UKF_states_array[:,0], 'r-o', label='UKF', lw = 1)
    
#TRASLACIONES#
if False:
    #GPS_TRANSLATION
    GPS_TRAS = []
    for i in range(len(GPS_signals)-1):
        x_i, y_i, _, _ = GPS_signals[i]
        x_f, y_f, _, _ = GPS_signals[i+1]
        d = np.sqrt((x_f-x_i)**2+(y_f-y_i)**2)
        GPS_TRAS.append(d)
    plt.plot( times_ukf[:(len(times_ukf)-1)], GPS_TRAS, 'b-')
    
    #ENC_TRANSLATION
    ENC_TRAS = []
    for i in range(len(ENC_signals)-1):
        tick_i = ENC_signals[i][1]
        tick_f = ENC_signals[i+1][1]
        d = ((tick_f-tick_i)*(1.4/13000))*10
        ENC_TRAS.append(d)
    plt.plot( times_enc[:len(ENC_TRAS)], ENC_TRAS, 'g-')
    ENC_VEL = [ENC_TRAS[i]/(times_enc[i+1]-times_enc[i])/10 for i in range(len(ENC_TRAS)-1)]
    #IMU_TRANSLATION
    IMU_TRAS = []
    aux_tras = 0
    for dt, vx, vy, vz, ax, ay, az  in IMU.rawdata: # range(len(IMU.rawdata)):
        aux_tras = (1*-ay*(dt)+0.5*-ay*((dt)**2))*np.cos(vy*dt)
        IMU_TRAS.append(aux_tras)
    plt.plot( times_imu, IMU_TRAS, 'y-')
    #UKF_TRANSLATION
    UKF_TRAS = []
    for i in range(UKF_states_array.shape[0]-1):
        UKF_TRAS.append(np.sqrt((UKF_states_array[i+1,0]-UKF_states_array[i,0])**2+(UKF_states_array[i+1,1]-UKF_states_array[i,1])**2))
    ind = min((len(times_ukf),len(UKF_TRAS)))
    plt.plot( times_ukf[:ind], UKF_TRAS[:ind], 'r-o')

#_POSITION
if True:
    plt.xlabel('Coordenada UTM (x)')
    plt.ylabel('Coordenada UTM (y)')
    plt.plot( GPS_signals_array[:,0], GPS_signals_array[:,1], 'b+', label='GPS', lw = 2)   # GPS
    plt.plot( UKF_states_array[:,0],  UKF_states_array[:,1] , 'r-', label='Filter',lw = 2 )   # UKF
    plt.plot( UKF_states_array[0,0],  UKF_states_array[0,1], 'ro', lw = 6 )   # initial filter point
    plt.legend(loc='upper left')
    ax = plt.subplot(111)
    ax.grid(clip_on=False, marker='+', markersize=2)
    plt.title(('source: '+ bagfile + ' | ' + 'data fusion'))
    plt.axis('equal')

#_ROTATION
if False:
    plt.title('Orientacion')
    plt.xlabel('Tiempo (seg)')
    plt.ylabel('Orientacion (rad)')
    plt.plot( times_ukf, GPS_signals_array[:,2], 'bo-', label='GPS', lw = 1)   # GPS
    plt.plot( times_imu, IMU_signals, 'go-', label='IMU', lw = 1)   # IMU
    plt.plot( times_ukf,  UKF_states_array[:,2], 'r-o', label='Filter',lw = 1 )   # UKF
    plt.legend(loc='upper left')
    ax = plt.subplot(111)
    ax.grid(clip_on=False, marker='+', markersize=2)

#_STEERING ANGLE
if False:
    plt.title('Angulo de direccion del vehiculo')
    plt.xlabel('Tiempo (seg)')
    plt.ylabel('Angulo de direccion (gra)')
    plt.plot( times_enc[:len(times_enc)+1], ENC_signals_array[:,0], 'b-o', label='ENC', lw = 1)
    plt.plot( times_ukf,  UKF_states_array[:,4] , 'r-o', label='Filter',lw = 1 )
    plt.legend(loc='upper left')
    ax = plt.subplot(111)
    ax.grid(clip_on=False, marker='+', markersize=2)

#_VELOCITY
if False:
    plt.title('Velocidad')
    plt.xlabel('Tiempo (seg)')
    plt.ylabel('Velocidad (m/s)')
    plt.plot( times_ukf, GPS_signals_array[:,3], 'b-o', label='GPS', lw = 2)
    plt.plot( times_enc[:(len(ENC_VEL))],  ENC_VEL, 'g-o', label='Odometria',lw = 1 )
    plt.plot( times_ukf,  UKF_states_array[:,3] , 'r-o', label='UKF',lw = 1 )
    plt.legend(loc='upper left')
    ax = plt.subplot(111)
    ax.grid(clip_on=False, marker='+', markersize=2)

#transl_x = [0.0302532584,0.0234393307,0.0246088973,0.0243968668,0.0127053543,0.0126075835,0.0124772095,0.0022534227,0.0022464773,0.0022017639,0.0531683198,0.0422746194,0.0006328345,0.0006304949,0.000646111,0.0015535024,0.0016334658,0.0015505033,0.0551501605,0.0460219065,0.0495600796,-0.0078914178,-0.0078011126,-0.0079758946,-0.017047891,-0.0168376722,-0.0170531879,0.0313914345,0.0283539217,0.0309973485,0.0006178171,0.0006486971,0.0006192006,0.0092890213,0.0087269725,-0.018275891,-0.0190385046,-0.0179456376,-0.0111161541,-0.0111635958,-0.010914085,0.0263091546,0.0329868968,0.0259966422,0.0006491258,0.0007684233,0.0006566199,0.0265485938,0.0234869699,0.0245570443,0.0296697636,0.0224384399,0.0260566564,0.0092129683,0.0090276052,0.0218270958,0.0090665814,0.0141569291,0.0427856151,0.0418770282,0.0429311744,0.0108785377,0.0138689497,0.0107950452,0.0209752782,0.0209996202,0.0212134663,0.0234752465,0.0234591772,0.022775839,0.0503798464,0.0477658782,0.0535375637,0.0003147664,0.0003014916,0.0003128541,-0.0003382049,-0.0003038445,0.0145577091,0.0137288391,0.0144211293,-0.000961888,-0.00116577,-0.0009640097,0.0126412734,0.0159128173,0.0125198793,-0.0195249177,-0.0197463474,-0.0196017815,0.0094856146,0.0090643431,0.0091536797,0.0120195473,0.0122671451,0.0118609245,0.0125517631,0.0094915531,0.0009528561,0.0009775872,0.000948433,0.0406702787,0.0415800888,0.0417993993,0.0190039396,0.0233584815,0.0191901009,-0.00606729,-0.0061420707,-0.0058049295,0.0006491445,0.0006387482,0.0006189463,0.0673685771,0.0539021504,0.0171996603,0.042599435,0.0050894796,-0.0108019523,-0.0100888658,-0.0105529113,0.0112823919,0.0102571845,0.0107553883,0.0057306871,0.0057604205,0.0057796865,0,0,0,0.0019632393,0.0015961912,0.0273863263,0.0273085509,0.0274280113,0.0495179387,0.0512667097,0.0478155556,-0.0118263352,-0.014705246,-0.0117833934,0.0554981253,0.054120653,0.0543362532,0,0,0,0.0541200767,0.0544211686,0.0538307395,-0.0153539863,-0.0119604536,0.0398825305,0.0390706531,0.0404879937,0.0097356948,0.0097210175,0.0100029898,0.056826024,0.0718535177,0.056742308,0.0093850833,0.0091593736,0.0092665969,-0.0144318525,-0.0147549783,-0.0142924905,0.1069038545,0.0089075949,0.043942477,-0.0710554295,-0.0585543823,-0.0220066024,-0.0278627119,-0.0210810029,-0.0019226904,-0.0018610979,-0.0019113414,0.0856987791,0.0861476653,0.0883059354,0.0015551842,0.0016144417,0.0015668275,0.0091282248,0.0094160501,0.0090375864,0.0587381642,0.0441048775,0.0452799851,-0.0092248343,-0.0093287053,0.0380165525,0.0380754759,0.037116547,0.0459816372,0.0414328463,0.0436364061,-0.1482682918,-0.1489281948,-0.1537532212,-0.1384290365,-0.1434222652,-0.1421545804,-0.0907463552,-0.0905675643,-0.0932765054,0.0404728155,0.0411956833,0.042464648,-0.0861857736,-0.0911533597,-0.0863450786,0.0679596029,0.0555183458,-0.1319718077,-0.1307261862,-0.1400005223,0.0396663532,0.0514601761,0.0434679505,-0.002862243,-0.0028672017,-0.0028496229,0.0233262643,0.0237617951,0.0245128065,0.0442070513,0.0449615718,0.0453418829,-0.0106001212,-0.0084777317,-0.0095232854,0.0520808188,0.0477205091,0.0498975878,-0.0574194903,-0.059856405,-0.0568207755,-0.0552995017,-0.0585713319,0.0302852298,0.031063738,0.0317419953,0.0072694287,0.0094328703,0.0078638613,0.0296836714,0.0308556259,0.030688922,0.0750000266,0.0756976209,0.0815065331,0.0930689911,0.0905923861,0.0935558166,-0.0577232727,-0.0592968199,-0.0576783139,0.0126800186,0.0124263216,0.1070499048,0.1064448884,0.1065945048,-0.0348276894,-0.031336318,-0.0335923424,-0.0034577871,-0.0035447283,-0.0035834362,-0.01119594,-0.0118871047,-0.0116360049,0.0430669601,0.0512632914,0.0426957749,-0.0438283967,-0.0346988714,-0.0363489501,-0.0047903708,-0.0047663511,0.0872582472,0.08290425,0.0856191505,0.0329377259,0.0330936171,0.0320100226,0.0533604661,0.0550949914,0.0541031865,0.0091314564,0.0092955779,0.0090545164,-0.0053932393,-0.0067853816,-0.0054843282,0.0550580115,0.0543831534,0.0534548203,-0.0308500071,-0.0314600789,-0.0310688203,-0.0266646137,-0.0258639125,0.0337355127,0.0422825697,0.0337478945,-0.0170070699,-0.0166439662,-0.0166841274,-0.0181357658,-0.0177713817,-0.0179265123,0.0047393066,0.0047693566,0.0047026019,-0.011941727,-0.0111830602,-0.0115413773,-0.0092453878,-0.0083562144,-0.0090060299,0.0277600024,0.0277423931,0.0276759684,-0.0047960686,-0.0046394189,0.0088758012,0.010988796]
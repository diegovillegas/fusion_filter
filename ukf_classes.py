import numpy as np
from ukf_functions import *
from constants import *
import skfuzzy as fuzz

from filterpy.common import dot3
from filterpy.kalman import unscented_transform
from filterpy.kalman import MerweScaledSigmaPoints as SigmaPoints

from scipy.linalg import inv, cholesky  #, sqrtm #sqrtm is other method to compute the squared root of a matrix

class sensor(object):
    def __init__( self, H_func=None, dim_x=5, dim_z=2 ):
        self.h_f   = H_func
        self.value = None
        self.rawdata = []
        self.preValue = self.value
        self.offset = None
        self.R     = None
        self.time = None # time when the recent measure has been read
        self.preTime = None # time when the previous measure was received
        self._meas_flg = False
        self._samples  = 0 
        self._dim_z = dim_z
        self._dim_x = dim_x
        self._num_sigmas = 2*self._dim_x + 1
        self.sigmas_h = np.zeros((self._num_sigmas, self._dim_z))
        self.x_gain = np.zeros_like(self._dim_x)
        self.P_gain = np.zeros( (self._dim_x, self._dim_x) )
        self.q = None
        self.S = None
        self.th = None
        self.status = True

    def valid_mf(self, input): # input like a vector
        return fuzz.trimf(input, [])

    # revisar!!!!!
    def update_data( self, data, R, time ):
        self.preTime = self.time
#        if self.offset is not None:
#            self.value = data - self.offset
#        else:
        self.value = data
        self.R     = R
        self.time = time

class filter_state(object):
    def __init__( self, dim_x, dim_z, dt, fx,
                 points_parameters= ( .5, 2, 0 ),
                 sqrt_fn=None, x_mean_fn=None, z_mean_fn=None,
                 residual_x=None, residual_z=None ):
        self.init_flag = False
        self.Q = np.eye(5)
        self.Q[0,0] = 3.5
        self.Q[1,1] = 3.5
        self.Q[2,2] = 2.0
        self.Q[3,3] = 2.5
        self.Q[4,4] = 2.5
        self.P = np.diag((100, 100, np.radians(0.027)*1.5, 2.5, v_steer*100))
        self.x = np.zeros(5)
        self.prev_x = np.zeros(5)
        self.x_p = np.zeros(5)
        self.P_p = np.zeros((5,5))
        self._dim_x = dim_x
        self._dim_z = dim_z
        self._dt = dt
        self._num_sigmas = 2*self._dim_x + 1
        self.fx = fx
        self.x_mean = x_mean_fn
        self.z_mean = z_mean_fn
        
        alpha, beta, kappa = points_parameters
        self.points_ = SigmaPoints( self._dim_x, alpha, beta, kappa )
        self.sigmas = np.zeros((self._num_sigmas, self._dim_x))
        # weights for the means and covariances.
        self.Wm, self.Wc = self.points_.weights()
        
        if sqrt_fn is None:
            self.msqrt = cholesky
        else:
            self.msqrt = sqrt_fn

        if residual_x is None:
            self.residual_x = np.subtract
        else:
            self.residual_x = residual_x

        if residual_z is None:
            self.residual_z = np.subtract
        else:
            self.residual_z = residual_z

        # sigma points transformed through f(x) and h(x)
        # variables for efficiency so we don't recreate every update
        self.sigmas_f = np.zeros((self._num_sigmas, self._dim_x))
    
    def predict( self, dt):
        if dt is None:
            dt = self._dt

        UT = unscented_transform

        # calculate sigma points for given mean and covariance
        self.sigmas = self.points_.sigma_points( self.x, self.P )

        for i in range( self._num_sigmas ):
            self.sigmas_f[i] = self.fx( self.sigmas[i], dt , fx_args=() )

        self.x_p, self.P_p = UT( self.sigmas_f, self.Wm, self.Wc, self.Q,
                            self.x_mean, self.residual_x )
        self.x_p[2] = normalize_angle(self.x_p[2])
        self.x_p[4] = normalize_steering(self.x_p[4])
    
    def calc_param( self, sensor, type_=0, fx_args=() ):
        #type: 0 --> global sensor,  1 --> local sensor
        if not isinstance(fx_args, tuple):
            fx_args = (fx_args,)
        R = sensor.R
        z = sensor.value
        prev_z = sensor.preValue
        if z is None:
            return
        UT = unscented_transform
        if R is None:
            R = self.R
        elif np.isscalar( R ):
            R = np.eye( self._dim_z ) * R
        if type_ == 1 : # is a local measure from IMU
            if sensor.preValue is not None:
                sensor.value += sensor.preValue
            else:
                sensor.preValue = sensor.value
        for i in range( sensor._num_sigmas ):
            sensor.sigmas_h[i] = sensor.h_f( self.sigmas_f[i], dt, prev_z )
        # mean and covariance of prediction passed through unscented transform
        zp, Pz = UT(sensor.sigmas_h, self.Wm, self.Wc, R,
                    self.z_mean, self.residual_z)
        # compute cross variance of the state and the measurements
        Pxz = np.zeros(( self._dim_x, sensor._dim_z ))
        for i in range(sensor._num_sigmas):
            dx = self.residual_x( self.sigmas_f[i], self.x_p )
#            print 'hi, i am ...dx=', dx, '\n fx_args=', fx_args, '\nTYPE=', type_
#            if type_==2:
#                dx += [fx_args[0][0], fx_args[0][1], 0,0,0]
            dz =  self.residual_z( sensor.sigmas_h[i], zp )
#            if type_==0:
#                print dz, '\n'
            Pxz += self.Wc[i] * np.outer( dx, dz )
        K = np.dot( Pxz, inv(Pz) )   # Kalman gain
        y = self.residual_z(z, zp)   #residual
        # compute the q_i to define the validity domain of sensor
        sensor.q = np.dot( y.T , np.dot( inv( Pz ) , ( y )) )
        sensor.S = Pz
        x_gain = np.dot( K, y )
        # normalizing ORIENTATION [2], STEERING [4] angles
        x_gain[2] = normalize_angle(x_gain[2])
        x_gain[4] = normalize_steering(x_gain[4])
        P_gain = dot3( K, Pz, K.T )
        return ( x_gain, P_gain )

    def update_state( self, x_gains, P_gains ):
        (x_gain_GPS, x_gain_ENC, x_gain_IMU) = x_gains
        (P_gain_GPS, P_gain_ENC, P_gain_IMU) = P_gains
        #print ('GPS: {}  ENC: {}  IMU: {}'.format(x_gain_GPS,x_gain_ENC,x_gain_IMU))
        # updating the system state
        self.x = self.x_p + x_gain_GPS + x_gain_ENC + x_gain_IMU
        self.P = inv( inv( self.P_p ) + ( P_gain_GPS + P_gain_ENC + P_gain_IMU) )
        self.x[2] = normalize_angle(self.x[2])
        self.prev_x = self.x

#!/usr/bin/env python
import numpy as np
import rosbag
import matplotlib.pyplot as plt
import glob

# to store outputs
GPS_x_gains, ENC_x_gains, IMU_x_gains = [], [], []
GPS_signals, ENC_signals, IMU_signals = [], [], []
ENC_outputs = []
UKF_states = []
auxiliar = []
# files
path_bagfile = '/media/diego/DataPart2/UC3M/2doCuatrimestre/Trabajo_LSI/data_icab/'

velobagfile_list = glob.glob(path_bagfile+'/velo_test*.bag')
mapbagfile_list = glob.glob(path_bagfile+'/map_test*.bag')

# select the rosbagfile
print 'velo_', len(velobagfile_list), '\n','map_', len(mapbagfile_list), '\n', 'Opening filebag...'
bagfile = velobagfile_list[4]
print '\n FILENAME :', bagfile

bag = rosbag.Bag((velobagfile_list[3]))

# rostopics of interest
if bagfile in velobagfile_list:
    topic_POS2 = '/icab1/mavros/global_position/global' # type: nav_msgs/Odometry
    topic_POS = '/icab1/mavros/global_position/local' # type: nav_msgs/Odometry
    topic_ENC = '/icab1/movement_manager/status_info' # type: icab_msgs/StatusInfo
    topic_IMU = '/icab1/mavros/imu/data'              # type: sensor_msgs/Imu
else:
    topic_POS2 ='/mavros/global_position/global' # type: nav_msgs/Odometry
    topic_POS = '/mavros/global_position/local' # type: nav_msgs/Odometry
    topic_ENC = '/movement_manager/status_info' # type: icab_msgs/StatusInfo
    topic_IMU = '/mavros/imu/data'              # type: sensor_msgs/Imu

imu_raw_data = []
acumdeg_ls, velocity, deg, secs, trans = [], [], [], [], []
acumdeg, desp = [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]
flag_started = False
time = []

for topic, msg, t in bag.read_messages():

    if topic == topic_IMU and flag_started:
        dt = t.to_sec() - init_time
        time.append(t.to_sec())
        secs.append(dt)
        deg.append([msg.angular_velocity.x, msg.angular_velocity.y, msg.angular_velocity.z])
        velocity.append([msg.linear_acceleration.x, msg.linear_acceleration.y, msg.linear_acceleration.z])
        desp[0] += msg.linear_acceleration.x*dt*dt
        desp[1] += msg.linear_acceleration.y*dt*dt
        desp[2] += msg.linear_acceleration.z*dt*dt
        trans.append(desp)
        degre = np.array([msg.angular_velocity.x, msg.angular_velocity.y, msg.angular_velocity.z] )
        acumdeg += dt*(degre -init_deg)
        acumdeg_ls.append(acumdeg)
        init_time = t.to_sec()
        init_deg = np.array([ msg.angular_velocity.x, msg.angular_velocity.y, msg.angular_velocity.z ])

    if topic == topic_IMU and not flag_started:
        init_time = t.to_sec()
        init_deg = np.array([ msg.angular_velocity.x, msg.angular_velocity.y, msg.angular_velocity.z ])
        flag_started = True
        print 'Started'

Deg = np.array(deg)
Acumdeg_ls = np.array(acumdeg_ls)
Secs = np.array(secs)
Trans = np.array(trans)
    
# ROTATION
if True:
    ax = plt.subplot(111)
    ax.grid(clip_on=False, marker='+', markersize=2)
    plt.title(('Fuente: '+ bagfile + ' | ' + 'IMU raw data'))
    #plt.axis('equal')
    plt.show()
    plt.xlabel('Tiempo (segundos)')
    plt.ylabel('Rotacion acumulada (grados)')
    plt.plot( range(len(secs)), Acumdeg_ls[:,0], 'r-+', label='IMU', lw = 2) 
    plt.plot( range(len(secs)), Acumdeg_ls[:,1], 'g-+', label='IMU', lw = 2) 
    plt.plot( range(len(secs)), Acumdeg_ls[:,2], 'b-+', label='IMU', lw = 1) 
    plt.legend(loc='upper left')

if False:
    plt.xlabel('Tiempo (segundos)')
    plt.ylabel('Rotacion acumulada (grados)')
    plt.plot( range(len(secs)), Deg[:,0], 'r-', label='IMU', lw = 1) 
    plt.plot( range(len(secs)), Deg[:,1], 'g-', label='IMU', lw = 1) 
    plt.plot( range(len(secs)), Deg[:,2], 'b-', label='IMU', lw = 1) 
    plt.legend(loc='upper left')

if False:
    plt.xlabel('Tiempo (segundos)')
    plt.ylabel('Rotacion acumulada (grados)')
    plt.plot( range(len(secs)), Trans[:,0], 'r-', label='IMU', lw = 1) 
    plt.plot( range(len(secs)), Trans[:,1], 'g-', label='IMU', lw = 1) 
    plt.plot( range(len(secs)), Arr_AdTrans[:,2], 'b-', label='IMU', lw = 1) 
    plt.legend(loc='upper left')



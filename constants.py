## CONSTANTES
import numpy as np
###### INITIAL NEEDED VALUES ######
dt = 0.050
v_steer = np.radians(0.087719298) #it might be bigger
# 0.08 deg/0.5seg . De 2280 medidas del sensor,
# la diferencia de la muestra k+1 con la k resulto en 5 grados.
# El transcurso de tiempo fueron de 114 segundos.
# con lo cual 5deg/114seg= .0877grad/0.5seg
v_rear_encoder = 18571.4285/10
# 13000ticks/(vuelta)*(1vuelta/1.4m)*(1m/0.5 seg) --> #18571.4285 
# a velocidad alta de 2m/seg.
prev_encoder = None # used for the F transition function
wheel_circ = 1.4 # arc of the circle in meters
ticks_per_rev = 13000
wheelbase = 1.66
prev_value_ticks = 0
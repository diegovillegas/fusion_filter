from math import tan, sin, cos
from filterpy.stats import plot_covariance_ellipse
from filterpy.kalman import MerweScaledSigmaPoints as SigmaPoints
import numpy as np
from filterpy.kalman import unscented_transform
from filterpy.kalman import UnscentedKalmanFilter as UKF
import matplotlib.pyplot as plt
from decimal import *
import pickle, glob, rosbag, math
#import Quaternion as Quat
'''
     state vector : x = [x, y, beta, vel, steering_angle]
     measurement  : z = [x , y]
'''
def state_mean(sigmas, Wm):
    x = np.zeros(len(sigmas[0]))

    sum_sin_or = np.sum(np.dot(np.sin(sigmas[:,4]), Wm))
    sum_cos_or = np.sum(np.dot(np.cos(sigmas[:,4]), Wm))
    sum_sin_steer = np.sum(np.dot(np.sin(sigmas[:,5]), Wm))
    sum_cos_steer = np.sum(np.dot(np.cos(sigmas[:,5]), Wm))
    x[0] = np.sum(np.dot(sigmas[:,0], Wm)) # x coord utm
    x[1] = np.sum(np.dot(sigmas[:,1], Wm)) # y coord utm
    x[2] = np.sum(np.dot(sigmas[:,2], Wm)) # vx
    x[3] = np.sum(np.dot(sigmas[:,3], Wm)) # vy
    x[4] = normalize_angle(np.math.atan2(sum_sin_or, sum_cos_or))
    x[5] = np.math.atan2(sum_sin_steer, sum_cos_steer)
    return x

def z_mean(sigmas, Wm):
    x = np.zeros(4)
    #sum_sin = np.sum(np.dot(np.sin(sigmas[:, 2]), Wm))
    #sum_cos = np.sum(np.dot(np.cos(sigmas[:, 2]), Wm))
    x[0] = np.sum(np.dot(sigmas[:,0], Wm)) # x
    x[1] = np.sum(np.dot(sigmas[:,1], Wm)) # y
    x[2] = np.sum(np.dot(sigmas[:,2], Wm)) # vel-x
    x[3] = np.sum(np.dot(sigmas[:,3], Wm)) # vel-y
    #x[2] = np.math.atan2(sum_sin, sum_cos)
    return x

def quat2angle(quat):
    roll   = math.atan2(2*(quat.w*quat.x+quat.y*quat.z),1-2*(quat.x**2+quat.y**2))
    pitch   = math.asin(2*(quat.w*quat.y+quat.z*quat.x))
    yaw   = math.atan2(2*(quat.w*quat.z+quat.x*quat.y),1-2*(quat.y**2+quat.z**2))
    return (roll, pitch, yaw)

def decimals(x, n):
    return np.float(np.round(x,n))

def normalize_angle(x):
    x = np.mod(x, 2*np.pi)
    if x > np.pi:
        x -= 2*np.pi
    return x

def add_angles(a,b):
    return math.atan2( (math.sin(a)+math.sin(b)), (math.cos(a)+math.cos(b)))

def subtract_x(a, b):
    y = a - b
    y[4] = normalize_angle(add_angles(a[4], -b[4]))
    y[5] = add_angles(a[5], -b[5])
    return y

def H_(x, hx_args):
    return np.array([x[0],x[1],x[2], x[3]])

def F__(x, dt): # x : [ x , y , velx , vely, hdg, steer ]
    beta = x[4]
    vx = x[2]
    vy = x[3]
    steering_angle = x[5]
    distx = vx*dt
    disty = vy*dt
    vel = np.sqrt(vx**2+vy**2)
    dist = np.sqrt(distx**2+disty**2)
    # turning
    if abs(steering_angle) > 0.1:
        dx_ = (vel**2)*(dt**2)*tan(steering_angle)/(2*wheelbase)
        dy_ = np.sqrt(dist**2-dx_**2)

        r = wheelbase/tan(steering_angle)
        dbeta = 2*np.math.atan2(dist/2, r)
    # going straight forward
    else:
        dbeta = 0
        dx_ = dist*sin(beta)
        dy_ = dist*cos(beta)
    return x + np.array([dx_, dy_, 0, 0, dbeta, 0])

def F_(x, dt):
    hdg = x[4]
    vx = x[2]
    vy = x[3]
    steering_angle = x[5]
    distx = vx*dt
    disty = vy*dt
    dist=np.sqrt(distx**2+disty**2)
    if abs(np.degrees(steering_angle)) > 0.1:
        beta = (dist/wheelbase)*np.tan(steering_angle)
        # the icc's radius r
        r = wheelbase / np.tan(steering_angle)
        sinh, sinhb = np.sin(hdg), np.sin(hdg + beta)
        cosh, coshb = np.cos(hdg), np.cos(hdg + beta)
        #x += np.array([-r*sinh + r*sinhb, r*cosh - r*coshb, 0, 0, beta, 0])
        return np.array([x[0]-r*sinh + r*sinhb , x[1] +r*cosh - r*coshb, x[2], x[3], normalize_angle(x[4]+ beta), x[5]])
    else:
        return x + np.array([distx, disty, 0, 0, 0, 0])
        
wheelbase = 1.66
x_, y_, beta_, cov = [], [], [], []
X_, P_ = [], []
timeukf = []
timeImu = []
timeEnc = []
steer_, velx_, vely_ = [], [], []
last_state, curr_steer, prev_steer = None, None, None
acumbx, acumby, acumbz = 0., 0., 0.
# to this must be included more bagfile's data.
dt_GPS = 0.58 # 580 ms
process_points = SigmaPoints(n=6, alpha=1, beta=2., kappa=-3) 
ukf = UKF( dim_x=6, dim_z=4, fx=F_, hx=H_, dt=None, 
          points=process_points, x_mean_fn=state_mean,
          residual_x=subtract_x, z_mean_fn=z_mean)

path_bagfile = '/media/diego/DataPart2/UC3M/2doCuatrimestre/Trabajo_LSI/data_icab/'
velofiles = glob.glob(path_bagfile + 'velo_test_1*.bag')
mapfiles  = glob.glob(path_bagfile + 'map_test*.bag')
velofiles.sort()
mapfiles.sort()
steer, vels = [], []
encoder = []
angles, anglesacum , anglesGPS = [], [], []
posx, posy = [] , []
flagInit, imuInit=False, False
flagInitGPS = False
for bagfile in velofiles[0:1]:
    # select the rosbagfile
    bag = rosbag.Bag((bagfile))
    print bagfile, 'is being read...'
    #for bagfile in mapfiles:
    # rostopics of interest depending on the bag file version
    if bagfile in velofiles:
        topic_POS2 = '/icab1/mavros/global_position/global' # type: nav_msgs/Odometry
        topic_POS = '/icab1/mavros/global_position/local' # type: nav_msgs/Odometry
        topic_ENC = '/icab1/movement_manager/status_info' # type: icab_msgs/StatusInfo
        topic_IMU = '/icab1/mavros/imu/data'              # type: sensor_msgs/Imu
    else:
        topic_POS2 ='/mavros/global_position/global' # type: nav_msgs/Odometry
        topic_POS = '/mavros/global_position/local' # type: nav_msgs/Odometry
        topic_ENC = '/movement_manager/status_info' # type: icab_msgs/StatusInfo
        topic_IMU = '/mavros/imu/data'              # type: sensor_msgs/Imu
    for topic_, msg_, t_ in bag.read_messages():
        if flagInit==False:
            lastTime = t_.to_sec()
            flagInit = True
            continue
        if topic_ == topic_POS:
            if flagInitGPS==False:
                lastTimeGPS = t_.to_sec()
                init_pos = [msg_.pose.pose.position.x.real, msg_.pose.pose.position.y.real]
                vel_x = msg_.twist.twist.linear.x
                vel_y = msg_.twist.twist.linear.y
                __, __, init_orientation = quat2angle(msg_.pose.pose.orientation)
                #START THE UKF#
                ukf.x = np.array([init_pos[0], init_pos[1],  vel_x, vel_y, init_orientation, 0])
                ukf.P = np.diag([500, 500, 3, 3, 10, 1])
                ukf.Q = np.diag((3.0, 3.0, 2, 2, 2.1, 1.5))
                flagInitGPS = True
            hx, hy, hz = quat2angle(msg_.pose.pose.orientation)
            currTime = t_.to_sec()
            x = msg_.pose.pose.position.x.real
            y = msg_.pose.pose.position.y.real
            vel_x = msg_.twist.twist.linear.x
            vel_y = msg_.twist.twist.linear.y
            var_x = msg_.pose.covariance[0] # error en x
            var_y = msg_.pose.covariance[7] # error en y
            v_o = msg_.pose.covariance[35]
            R = np.diag((var_x, var_y, 3, 3))
            #-----PREDICT------#
            print "dtime ukf : {}".format(currTime - lastTimeGPS)
            print("Before prediction : {}\n".format(ukf.x))
            ukf.predict(dt = currTime - lastTimeGPS)#0.0520)
            print("After prediction : {}\n".format(ukf.x))
            #-----UPDATE------#
            ukf.update([x, y, vel_x, vel_y], R = R, hx_args=())
            print("After update : {}\n".format(ukf.x))
            if last_state is not None:
                timeukf.append(t_.to_sec())
                anglesGPS.append( (hx, hy, hz ))
                x_.append(ukf.x[0])
                y_.append(ukf.x[1])
                beta_.append(+ ukf.x[4])
                velx_.append(ukf.x[2])
                vely_.append(ukf.x[3])
                steer_.append(ukf.x[5])
                P_.append(ukf.P)
                posx.append(x)
                posy.append(y)
                vels.append(( msg_.twist.twist.linear.x, msg_.twist.twist.linear.y, msg_.twist.twist.linear.z ))
            last_state = ukf.x
            lastTimeGPS = currTime
        if topic_ == topic_ENC :
            if prev_steer is not None:
                delta_steer = curr_steer - prev_steer
                #ukf.x[4] = delta_steer
            curr_steer = msg_.dir.angle.real
            steer.append(msg_.dir.angle.real)
            encoder.append(msg_.trc.val_encoder.real)
            timeEnc.append(t_.to_sec())
            prev_steer = curr_steer
        if topic_ == topic_IMU:
            if imuInit==False:
                prevTimeIMU = t_.to_sec()
                imuInit = True
            else:
                dt = t_.to_sec() - prevTimeIMU
                b_x, b_y, b_z = quat2angle( msg_.orientation )
                acumbx += (dt*msg_.angular_velocity.x)
                acumby += (dt*msg_.angular_velocity.y)
                acumbz += (dt*msg_.angular_velocity.z)
                #init_orientation += dt*b_z
                anglesacum.append((acumbx, acumby, acumbz))
                angles.append((+b_x, +b_y, +b_z))
                timeImu.append(t_.to_sec())
                prevTimeIMU = t_.to_sec()
#%%
P_ = np.array(P_)
posx = np.array(posx)
posy = np.array(posy)
steer = np.array(steer)
encoder = np.array(encoder)
vels = np.array(vels)
angles = np.array(angles)
anglesGPS = np.array(anglesGPS)
anglesacum = np.array(anglesacum)
beta_=np.array(beta_)
velx_=np.array(velx_)
#%%
plt.figure(1)
plt.subplot(221)
plt.plot(posx, posy, 'b+',label='GPS', lw = 1) # data
plt.plot(x_, y_, 'r-', label='UKF',lw = 2) # filter
plt.legend(loc='upper left')
plt.axis('equal')

plt.subplot(222)
#plt.plot(timeImu, angles[:,0], 'g+',label='ORIENTATION (IMU)', lw = 1) # roll data
#plt.plot(timeImu, angles[:,1], 'r+',label='ORIENTATION (IMU)', lw = 1) # pitch data
plt.plot(timeImu, angles[:,2], 'b-',label='ORIENTATION (IMU-quat)', lw = 1) # yaw data
plt.plot(timeImu, init_orientation+anglesacum[:,2], 'g-',label='ORIENTATION (IMU-angularveloc)', lw = 1) # data
plt.plot(timeukf, anglesGPS[:,2], 'y-',label='ORIENTATION (GPS)', lw = 1) # data
plt.plot(timeukf, init_orientation+beta_[:], 'r-', label='UKF', lw = 2) # filter
plt.legend(loc='upper left')

plt.subplot(224)
plt.plot(timeEnc, steer, 'b-',label='STEER (ENCODER)', lw = 1) # data
plt.plot(timeukf, steer_, 'r-', label='steering', lw = 2) # filter
plt.legend(loc='upper left')
plt.title(('source: '+ bagfile + ' | ' + 'topic: ' + topic_))

plt.figure(2)
plt.subplot(121)
plt.plot(range(len(timeukf)), velx_, 'r-', label='UKF-velocidad-x', lw = 2) # filter
plt.plot(range(len(timeukf)), vels[:, 0], 'b-', label='GPS-velocidad-x', lw = 2) # filter
plt.legend(loc='upper left')
#plt.axis('equal')

plt.subplot(122)
plt.plot(range(len(timeukf)), vely_, 'r-', label='UKF-velocidad-y', lw = 2) # filter
plt.plot(range(len(timeukf)), vels[:, 1], 'b-', label='GPS-velocidad-y', lw = 2) # filter
plt.legend(loc='upper left')
#plt.axis('equal')


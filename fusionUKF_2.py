from math import tan, sin, cos
from filterpy.stats import plot_covariance_ellipse
from filterpy.kalman import MerweScaledSigmaPoints as SigmaPointsM
from filterpy.kalman import JulierSigmaPoints as SigmaPointsJ
from filterpy.kalman import SimplexSigmaPoints as SigmaPointsS
import numpy as np
from filterpy.kalman import unscented_transform
from filterpy.kalman import UnscentedKalmanFilter as UKF
import matplotlib.pyplot as plt
from filterpy.common import dot3
from scipy.linalg import inv
from decimal import Decimal
import glob, rosbag, math

'''
     state vector : x = [x, y, velx, vely, beta, steering_angle] : [m, m, m/s, m/s, gra, gra]
     measurement  : z_gps = [x , y, velx, vely] : [m, m, m/s, m/s]
     measurement  : z_imu = [wx , wy, wz] (roll, pitch, yaw) :[gra/s]X3
'''

UKF_PERIOD = 0.52#0.05157 # period of Kalman filter (seg)
FUKF2_PERIOD = 0.5
VAR_STEER = 0.087719298
wheelbase = 1.66
dt_GPS = 0.58 # 580 ms

#=================================================#

class sensor(object):
    def __init__( self, H_func, dim_z, dim_x, th):
        self.h_f   = H_func

        self.initValue = None
        self.value = None
        self.preValue = None
        self.acValue = None

        self.initTime = None
        self.time = None    # time when the recent measurement has been read
        self.preTime = None # time when the previous measurement was received

        self.rawdata = []
        self.offset = None
        self.R     = None
        self.meas_flg = False # either the sensor is being read or not
        self.samples  = 0
        self.dim_z = dim_z
        self.num_sigmas = 2*dim_x + 1
        self.sigmas_h = np.zeros((self.num_sigmas, self.dim_z))
        self.x_gain = np.zeros(dim_x)
        self.P_gain = np.zeros((dim_x, dim_x))
        self.q = None   # estadistico que sigue una distribucion ji-cuadrado
        self.S = None
        self.th = None  # threshold to decide if the sensor will be included in the fusion
        self.status = True # either the sensor passed the level required specified by self.th

    def read_data( self, data, R, time ):
        if self.samples==0:
            self.init_sensor( data, R, time)
        else:
            self.update_data( data, R, time )
        
    def init_sensor( self, data, R, time ):
        self.initTime = time
        self.initValue = data
        self.value = data
        self.acValue = self.initValue
        self.samples +=1
        print('Sensor started!\n')

    def update_data( self, data, R, time ):
        self.preTime = self.time
        self.preValue = self.value
        self.value = data
        #self.acValue += self.value
        self.time = time
        self.R    = R
        self.samples +=1

#=======================================================#

class FUKF(object):
    def __init__( self, dim_x, dt, fx,
                 points,
                 sqrt_fn=None, x_mean_fn=None, #z_mean_fn=None,
                 residual_x=None, residual_z=None ):
        self.init_flag = False
        self.states = []
        self.x = None
        self.P = None
        self.Q = None
        self.dim_x = dim_x
        self.prev_x = None
        self.x_p = np.zeros(self.dim_x)
        self.P_p = np.zeros((self.dim_x,self.dim_x))
        self._dt = dt
        self.num_sigmas = 2*self.dim_x + 1
        self.fx = fx
        self.x_mean = x_mean_fn
        self.points_ = points
        # weights for the means and covariances.
        self.Wm, self.Wc = self.points_.weights()
        
        if sqrt_fn is None:
            self.msqrt = cholesky
        else:
            self.msqrt = sqrt_fn

        if residual_x is None:
            self.residual_x = np.subtract
        else:
            self.residual_x = residual_x

        if residual_z is None:
            self.residual_z = np.subtract
        else:
            self.residual_z = residual_z

        # sigma points transformed through f(x) and h(x)
        # variables for efficiency so we don't recreate every update
        self.sigmas_f = np.zeros((self.num_sigmas, self.dim_x))
    
    def predict( self, dt, fx_args=()):
        if not isinstance(fx_args, tuple):
            fx_args = (fx_args,)
        if dt is None:
            dt = self._dt

        UT = unscented_transform

        # calculate sigma points for given mean and covariance
        sigmas = self.points_.sigma_points( self.x, self.P )
        for i in range( self.num_sigmas ):
            self.sigmas_f[i] = self.fx( sigmas[i], dt , *fx_args )

        self.x_p, self.P_p = UT( self.sigmas_f, self.Wm, self.Wc, self.Q,
                            self.x_mean, self.residual_x )
        # normalize the components with angles?

    def calc_param( self, sensor, fx_args=() ):
        if not isinstance(fx_args, tuple):
            fx_args = (fx_args,)
        R = sensor.R
        z = sensor.value
        if z is None:
            return
        UT = unscented_transform
        if R is None:
            return
        elif np.isscalar( R ):
            R = np.eye( sensor.dim_z ) * R

        for i in range( sensor.num_sigmas ):
            sensor.sigmas_h[i] = sensor.h_f( self.sigmas_f[i], fx_args )
        # mean and covariance of prediction passed through unscented transform
        zp, Pz = UT( sensor.sigmas_h, self.Wm, self.Wc, R )
                    #z_mean, residual_z )
        # compute cross variance of the state and the measurements
        Pxz = np.zeros(( self.dim_x, sensor.dim_z ))
        for i in range(sensor.num_sigmas):
            dx = self.residual_x( self.sigmas_f[i], self.x_p )
            dz = self.residual_z( sensor.sigmas_h[i], zp )
            Pxz += self.Wc[i] * np.outer( dx, dz )
        K = np.dot( Pxz, inv(Pz) )   # Kalman gain
        y = self.residual_z(z, zp)   #residual
        # compute the q_i to define the validity domain of sensor
        sensor.q = np.dot( y.T , np.dot( inv( Pz ) , ( y )) )
        sensor.S = Pz
        x_gain = np.dot( K, y )
        P_gain = dot3( K, Pz, K.T )
        return ( x_gain, P_gain )

    def update_state( self, x_gains, P_gains ):
        (x_gain_GPS, x_gain_ENC, x_gain_ENCSTEER, x_gain_IMU) = x_gains
        (P_gain_GPS, P_gain_ENC, P_gain_ENCSTEER, P_gain_IMU) = P_gains
        #print ('GPS: {}  ENC: {}  IMU: {}'.format(x_gain_GPS,x_gain_ENC,x_gain_IMU))
        # updating the system state
        self.x = self.x_p + x_gain_GPS + x_gain_ENC + x_gain_ENCSTEER + x_gain_IMU
        self.P = inv( inv( self.P_p ) + ( P_gain_GPS + P_gain_ENC + P_gain_ENCSTEER + P_gain_IMU) )
        #self.x[4] = normalize_angle(self.x[4]) #normalize angles?
        self.prev_x = self.x

#=======================================================#

def state_mean(sigmas, Wm):
    x = np.zeros(len(sigmas[0]))
    sum_sin_or = 0. #np.sum(np.dot(np.sin(sigmas[:,4]), Wm*0.01))
    sum_cos_or = 0. #np.sum(np.dot(np.cos(sigmas[:,4]), Wm*0.01))
    sum_sin_steer = 0. #np.sum(np.dot(np.sin(sigmas[:,5]), Wm))
    sum_cos_steer = 0. #np.sum(np.dot(np.cos(sigmas[:,5]), Wm))
    for i in range(len(sigmas)):
        s = sigmas[i]
        x[0] += s[0] * Wm[i] # x coord utm
        x[1] += s[1] * Wm[i]  # y coord utm
        x[2] += s[2] * Wm[i]  # vx
        x[3] += s[3] * Wm[i]  # vy
        sum_sin_or +=  np.sin(s[4]) * Wm[i]
        sum_cos_or +=  np.cos(s[4]) * Wm[i]
        sum_sin_steer +=  np.sin(s[5]) * Wm[i]
        sum_cos_steer +=  np.cos(s[5]) * Wm[i]
    x[4] = np.math.atan2(sum_sin_or, sum_cos_or)
    x[5] = np.math.atan2(sum_sin_steer, sum_cos_steer)
    return x

def z_mean(sigmas, Wm):
    x = np.zeros(4)
    #sum_sin = np.sum(np.dot(np.sin(sigmas[:, 2]), Wm))
    #sum_cos = np.sum(np.dot(np.cos(sigmas[:, 2]), Wm))
    x[0] = np.sum(np.dot(sigmas[:,0], Wm))
    x[1] = np.sum(np.dot(sigmas[:,1], Wm))
    x[2] = np.sum(np.dot(sigmas[:,2], Wm))
    x[3] = np.sum(np.dot(sigmas[:,3], Wm))
    #x[2] = np.math.atan2(sum_sin, sum_cos)
    return x

def quat2angle(quat):
    roll   = math.atan2(2*(quat.w*quat.x+quat.y*quat.z),1-2*(quat.x**2+quat.y**2))
    pitch   = math.asin(2*(quat.w*quat.y+quat.z*quat.x))
    yaw   = math.atan2(2*(quat.w*quat.z+quat.x*quat.y),1-2*(quat.y**2+quat.z**2))
    return (roll, pitch, yaw)

def decimals(x, n):
    return np.float(np.round(x,n))

def normalize_angle(x):
    x = np.mod(x, 2*np.pi)
    if x > np.pi:
        x -= 2*np.pi
    return x

def subtr_angles(a,b):
    return math.atan2( (math.sin(a)+math.sin(b)), (math.cos(a)+math.cos(b)))

def subtract_x(a, b):
    y = a - b
    #y[4] = subtr_angles(a[4], -b[4])
    #y[5] = subtr_angles(a[5], -b[5])
    return y

def H_(x, hx_args=()):
    return x

def H_gps(x, hx_args=()):
    return np.array([x[0],x[1],x[2], x[3]])

def H_imu(x, hx_args=()):
    return np.array([x[4]])

def H_encSteer(x, hx_args=()):
    return np.array([x[5]])

def H_odometry(x, hx_args ):
    """
        INPUT -> x = [x, y, vx, vy, orient, steer] 
        OUTPUT-> [newX, newY, newVelX, newVelY]
    """
    return np.array([ 0, 0, x[2], x[3] ])

def odometry( hdg, steering_angle, enc, dt):
    """
        INPUTS-> 
              -> hdg, steering_angle 
              -> enc = [ticks]
        OUTPUT-> [0, 0, newVelX, newVelY]
    """
    #print("time {} \n dsteer {} \n dticks {}\n".format( dt, steering_angle, enc))
    aux = Decimal(enc)/Decimal(13000)
    dist = float(aux)*1.4
    vel = dist*dt
    velx = vel*np.cos(hdg)/dt
    vely = vel*np.sin(hdg)/dt
    return np.array([0 , 0, velx, vely ])
    
def F__(x, dt):
    beta = x[4]
    vx = x[2]
    vy = x[3]
    steering_angle = x[5]
    distx = vx*dt
    disty = vy*dt
    dist=np.sqrt(distx**2+disty**2)
    vel=np.sqrt(vx**2+vy**2)
    # turning
    if abs(steering_angle) > np.radians(10):
        dx_ = (vel**2)*(dt**2)*tan(steering_angle)/(2*wheelbase)
        dy_ = np.sqrt(dist**2-dx_**2)
        Rot = np.array([[cos(beta), -sin(beta)],
                        [sin(beta),  cos(beta)]])
        result = np.cross(Rot, np.array([dx_, dy_]).T)
        dx_=result[0]
        dy_=result[1]
        r = wheelbase/tan(steering_angle)
        dbeta = normalize_angle((dist/wheelbase)*np.tan(steering_angle))
        #dbeta = normalize_angle(2*np.math.atan2(dist/2, r))
    # going straight forward
    else:
        dbeta = 0
        dx_ = dist*(math.cos(beta))
        dy_ = dist*(math.sin(beta))
    return x + np.array([dx_, dy_, 0, 0, dbeta, 0])

def F_(x, dt):
    hdg = x[4]
    vx = x[2]
    vy = x[3]
    steering_angle = x[5]
    distx = vx*dt
    disty = vy*dt
    dist=np.sqrt(distx**2+disty**2)
    if abs(steering_angle) > 0.5:
        beta = (dist/wheelbase)*np.tan(steering_angle)
        beta = normalize_angle(beta)
        #print("hdg ={}   |   beta ={}  \n".format(hdg, beta))
        # the icc's radius r
        r = wheelbase / np.tan(steering_angle)
        sinh, sinhb = np.sin(hdg), np.sin(hdg + beta)
        cosh, coshb = np.cos(hdg), np.cos(hdg + beta)
        x += np.array([-r*sinh + r*sinhb, r*cosh - r*coshb, 0, 0, np.radians(beta), 0])
        return x
    else:
        return x + np.array([distx, disty, 0, 0, 0, 0])

def F___(x, dt):
    hdg = x[4]
    vx = x[2]
    vy = x[3]
    steering_angle = x[5]
    distx = vx*dt
    disty = vy*dt
    dist=np.sqrt(distx**2+disty**2)
    if abs(steering_angle) > 0.01:
        beta = (dist/wheelbase)*np.tan(steering_angle)
        #beta = normalize_angle(beta)
        #print("hdg ={}   |   beta ={}  \n".format(hdg, beta))
        # the icc's radius r
        r = wheelbase / np.tan(steering_angle)
        sinh, sinhb = np.sin(hdg), np.sin(hdg + beta)   
        cosh, coshb = np.cos(hdg), np.cos(hdg + beta)
        x += np.array([-r*sinh + r*sinhb, r*cosh - r*coshb, 0, 0, (beta), 0])
        return x
    else:
        return x + np.array([distx, disty, 0, 0, 0, 0])

def add_angles(a,b):
    return math.atan2( (math.sin(a)+math.sin(b)), (math.cos(a)+math.cos(b)))

#=======================================================#


#def main():

#Variables Flags y dataStorage.
x_, y_, beta_, states_vector_1, states_vector_2 = [], [], [], [], []
x_2, y_2, beta_2 = [], [], []
X_, P_, X_2, P_2, P_fukf = [], [], [], [], []
timefukf1 = []
timefukf2 = []
timeGps = []
timeImu = []
timeEnc = []
steer_, velx_, vely_ = [], [], []
steer_2, velx_2, vely_2 = [], [], []
last_state, curr_steer, prev_steer = None, None, None
curr_ticks, prev_ticks = None, None
acumbx, acumby, acumbz = 0., 0., 0.
steer, vels = [], []
encoder, odometryData = [], []
odomx, odomy, odomvx, odomvy = 0, 0, 0, 0
angles, anglesacum , anglesGPS = [], [], []
posx, posy = [] , []
flagInit, imuInit=False, False
flagInitGPS = False
flagInitUKF = False
init_pos, vel_x, vel_y, init_orientation = None, None, None, None

process_points_fukf = SigmaPointsM(n=6, alpha=0.01, beta=2, kappa=-2) #SigmaPoints for the process model.
#process_points_fukf = SigmaPointsM(n=6, alpha=10, beta=2, kappa=-3) #SigmaPoints for the process model.
process_points_fukf2 = SigmaPointsM(n=6, alpha=1.3, beta=2, kappa=-1) #SigmaPoints for the process model.   kappa=0.001)#

# Define UKF ***********
fukf1 = UKF( dim_x=6, dim_z=4, fx=F__, hx=H_gps, dt=0.52,
          points=process_points_fukf)

# Define FUSION UKF ***********
fukf2 = UKF( dim_x=6, dim_z=6, fx=F__, hx=H_, dt=0.52,
          points=process_points_fukf2, x_mean_fn=state_mean)

# Define sensors
gps      = sensor(H_gps, dim_z=4, dim_x=6, th=3)
odom     = sensor(H_odometry, dim_z=4, dim_x=6, th=2)
encSteer = sensor(H_encSteer, dim_z=1, dim_x=6, th=2)
imu      = sensor(H_imu, dim_z=1, dim_x=6, th=2)

# Locate the raw data to offline processing 
path_bagfile = '/media/diego/DataPart2/UC3M/2doCuatrimestre/Trabajo_LSI/data_icab/'
velofiles = glob.glob(path_bagfile + 'velo_test_1*.bag')
mapfiles  = glob.glob(path_bagfile + 'map_test*.bag')
velofiles.sort()
mapfiles.sort()
prevHz = None
#reading the bagfile
for bagfile in velofiles[0:1]:
    # select the rosbagfile
    bag = rosbag.Bag((bagfile))
    print 'The bagfile {} will be open.'.format(bagfile)
    # rostopics of interest depending on the bag file version
    if bagfile in velofiles:
        topic_POS2 = '/icab1/mavros/global_position/global' # type: nav_msgs/Odometry
        topic_POS = '/icab1/mavros/global_position/local' # type: nav_msgs/Odometry
        topic_ENC = '/icab1/movement_manager/status_info' # type: icab_msgs/StatusInfo
        topic_IMU = '/icab1/mavros/imu/data'              # type: sensor_msgs/Imu
    else:
        topic_POS2 ='/mavros/global_position/global' # type: nav_msgs/Odometry
        topic_POS = '/mavros/global_position/local' # type: nav_msgs/Odometry
        topic_ENC = '/movement_manager/status_info' # type: icab_msgs/StatusInfo
        topic_IMU = '/mavros/imu/data'              # type: sensor_msgs/Imu
    for topic_, msg_, t_ in bag.read_messages():
        if flagInit==False:
            lastTime = t_.to_sec()
            flagInit = True
            continue
        if topic_ == topic_POS:
            lastTimeGPS = t_.to_sec()
            init_pos = [msg_.pose.pose.position.x.real, msg_.pose.pose.position.y.real]
            vel_x = msg_.twist.twist.linear.x
            vel_y = msg_.twist.twist.linear.y
            if gps.samples ==0:
                __, __, init_orientation = quat2angle(msg_.pose.pose.orientation)
                dataOdom = np.array([float(init_pos[0]), float(init_pos[1]), vel_x, vel_y])
            hx, hy, hz = quat2angle(msg_.pose.pose.orientation)
            if prevHz is not None and (abs(hz) - abs(prevHz) > 2*np.pi) :
                    hz += sign(prevHz)*2*np.pi
            currTime = t_.to_sec()
            pos_x = msg_.pose.pose.position.x.real
            pos_y = msg_.pose.pose.position.y.real
            vel_x = msg_.twist.twist.linear.x
            vel_y = msg_.twist.twist.linear.y
            var_x = msg_.pose.covariance[0] # error en x
            var_y = msg_.pose.covariance[7] # error en y
            v_o = msg_.pose.covariance[35]
            R = np.diag((var_x, var_y, 3, 3))
            posx.append(pos_x)
            posy.append(pos_y)
            vels.append(( msg_.twist.twist.linear.x, msg_.twist.twist.linear.y, msg_.twist.twist.linear.z ))
            anglesGPS.append((hx, hy, hz))
            prevHz = hz
            timeGps.append(t_.to_sec())
            
            gps.read_data(data=np.array([pos_x, pos_y, vel_x, vel_y]), R=R, time=t_.to_sec())
        # ---------ENCODERS-----------------------------------------#
        if topic_ == topic_ENC and imu.samples>0:
            if prev_steer is not None:
                curr_steer = msg_.dir.angle.real
                curr_ticks = msg_.trc.val_encoder.real
                delta_steer = curr_steer - prev_steer
                delta_ticks = curr_ticks - prev_ticks
                delta_t     = t_.to_sec() - prev_time_enc
                steer.append(msg_.dir.angle.real-prev_steer)
                timeEnc.append(t_.to_sec())
                encoder.append(msg_.trc.val_encoder.real)
                encSteer.read_data(data=delta_steer, R=VAR_STEER, time=t_.to_sec())
                #print 'delta_ticks {}'.format(delta_ticks)
                nDataOdom = odometry( hdg=fukf1.x[4], steering_angle=encSteer.value, enc=delta_ticks, dt=delta_t )
                dataOdom[0] += nDataOdom[0]
                dataOdom[1] += nDataOdom[1]
                dataOdom[2] = nDataOdom[2]
                dataOdom[3] = nDataOdom[3]
                #print("dataOdom : {}  \n time {} \n dsteer {} \n dticks {}\n".format(dataOdom, delta_t, delta_steer, delta_ticks))
                odomx, odomy, odomvx, odomvy = dataOdom
                odometryData.append((odomx, odomy, odomvx, odomvy))
                odom.read_data(data=dataOdom, R=np.diag((10, 10, 10, 10)), time=t_.to_sec())
                prev_steer = curr_steer
                prev_ticks = curr_ticks
                prev_time_enc = t_.to_sec()
            else:
                prev_steer = msg_.dir.angle.real
                prev_ticks = msg_.trc.val_encoder.real
                prev_time_enc = t_.to_sec()
        # ---------IMU----------------------------------------------#
        if topic_ == topic_IMU  and gps.samples>0:
            if imuInit==False:
                prevTimeIMU = t_.to_sec()
                acumbz = init_orientation
                imuInit = True
            else:
                dt = t_.to_sec() - prevTimeIMU
                b_x, b_y, b_z = quat2angle( msg_.orientation )
                acumbx += (dt*msg_.angular_velocity.x)
                acumby += (dt*msg_.angular_velocity.y)
                acumbz += (dt*msg_.angular_velocity.z)
                anglesacum.append((acumbx, acumby, acumbz))
                angles.append((+b_x, +b_y, +b_z))
                imu.read_data(data=acumbz, R=10, time=t_.to_sec())
                timeImu.append(t_.to_sec())
                prevTimeIMU = t_.to_sec()
        #-----------------------------------------------------------#
        #-------.. STARTING FILTERS FUSION UKF ..-------------------#
        #-----------------------------------------------------------#
        if init_pos is not None and vel_x is not None and vel_y is not None and \
            init_orientation is not None and not flagInitUKF and gps.samples>2 and \
            encSteer.samples>2 and imu.samples>2 and odom.samples>2:

            # initial state vector
            # [pos_x, pos_y, vel_x, vel_y, global_orientation, steering_angle]
            x = np.array([init_pos[0], init_pos[1],  vel_x, vel_y, init_orientation, 0])

            #initialize Fusion-UKF #1
            print("INIT STATE X:{} \n".format(x))
            fukf1.x = x
            fukf1.P = np.diag([100, 100, 2, 2, 0.01, 0.1])
            fukf1.Q = np.diag((.5, .5, .1, .1, .5, 1))
            time_fukf = t_.to_sec()
            print("FUKF 1 STARTED!")

            #initialize Fusion-UKF #2
            time_fukf2 = t_.to_sec()
            fukf2.x = x
            fukf2.P = np.diag([10, 10, 2, 2, 3, 5])
            fukf2.Q = np.diag((2, 2, .5, .5, 30, 5))
            print("FUKF 2 STARTED!")
            flagInitUKF = True
        if flagInitUKF:
        #############################################################
        #---------------------- F U K F   2 ------------------------#
        #############################################################
            if (Decimal(t_.to_sec()) - Decimal(time_fukf2) >= FUKF2_PERIOD):
                #======================================================#
                #---------------- P R E D I C T -----------------------#
                #======================================================#
                fukf2.predict(dt = t_.to_sec() - time_fukf2) # FUSION FILTER : METHOD B
                time_fukf2 = t_.to_sec()

                #======================================================#
                #--------------- U P D A T E --------------------------#
                #======================================================#
                fukf2.update(np.array([gps.value[0], gps.value[1], vel_x, vel_y, acumbz, delta_steer]), R= np.diag((var_x, var_y, 5, 5, 1.5, VAR_STEER)), hx_args=())

                #-----SAVING SIGNAL TO PLOT-------#
                timefukf2.append(t_.to_sec())
                x_2.append(fukf2.x[0])
                y_2.append(fukf2.x[1])
                beta_2.append( fukf2.x[4])
                velx_2.append(fukf2.x[2])
                vely_2.append(fukf2.x[3])
                steer_2.append(fukf2.x[5])
                P_2.append(fukf2.R)
                states_vector_2.append(fukf2.x)
                
        #############################################################
        #---------------------- F U K F   1 ------------------------#
        #############################################################
"""            if Decimal(t_.to_sec()) == Decimal(gps.time):
                fukf1.hx = H_gps
                #-----PREDICT------#
                fukf1.predict(dt = gps.time - gps.preTime)

                #-----UPDATE-------#
                fukf1.update(z=gps.value, R=gps.R, hx_args=())

                #-----SAVING SIGNAL TO PLOT-------#
                timefukf1.append(t_.to_sec())
                x_.append(fukf1.x[0])
                y_.append(fukf1.x[1])
                beta_.append( (fukf1.x[4]))
                velx_.append(fukf1.x[2])
                vely_.append(fukf1.x[3])
                steer_.append(fukf1.x[5])
                P_.append(fukf1.P.diagonal())
                states_vector_1.append(fukf1.x)
                
                time_fukf = t_.to_sec()

            if Decimal(t_.to_sec()) == Decimal(imu.time):
                fukf1.hx = H_imu
                #--------------- P R E D I C T ---------------#
                fukf1.predict(dt = 0.1) #imu.time - imu.preTime)
                #180ms : IMU frecuency rate 

                #-------------- U P D A T E ------------------#
                fukf1.update(z=imu.value, R=imu.R, hx_args=())

                #-----SAVING SIGNAL TO PLOT-------#
#                timefukf1.append(t_.to_sec())
#                x_.append(fukf1.x[0])
#                y_.append(fukf1.x[1])
#                beta_.append( (fukf1.x[4]))
#                velx_.append(fukf1.x[2])
#                vely_.append(fukf1.x[3])
#                steer_.append(fukf1.x[5])
#                P_.append(fukf1.P.diagonal())

            if Decimal(t_.to_sec()) == Decimal(encSteer.time):
                fukf1.hx = H_encSteer
                #--------------- P R E D I C T ---------------#
                fukf1.predict(dt = encSteer.time - encSteer.preTime)
                #50ms : ENCODER frecuency rate 

                #-------------- U P D A T E ------------------#
                fukf1.update(z=encSteer.value, R=encSteer.R, hx_args=())

                #-----SAVING SIGNAL TO PLOT-------#
#                timeukf.append(t_.to_sec())
#                x_.append(fukf1.x[0])
#                y_.append(fukf1.x[1])
#                beta_.append( (fukf1.x[4]))
#                velx_.append(fukf1.x[2])
#                vely_.append(fukf1.x[3])
#                steer_.append(fukf1.x[5])
#                P_.append(fukf1.P.diagonal())

            if (Decimal(t_.to_sec()) - Decimal(odom.time) == Decimal(0.0)):
                fukf1.hx = H_odometry
                #-----PREDICT------#
                fukf1.predict(dt = odom.time - odom.preTime)

                #-----UPDATE-------#
                fukf1.update(z=odom.value, R=odom.R, hx_args=())

#                #-----SAVING SIGNAL TO PLOT-------#
#                timeukf.append(t_.to_sec())
#                x_.append(fukf1.x[0])
#                y_.append(fukf1.x[1])
#                beta_.append( (fukf1.x[4]))
#                velx_.append(fukf1.x[2])
#                vely_.append(fukf1.x[3])
#                steer_.append(fukf1.x[5])
#                P_.append(fukf1.P.diagonal())

#                if odom.q > odom.th:
#                    #print "odom.q", odom.q
#                    odom.status = False
#                    if odom.samples % 100 ==0:
#                        print 'ODOM SIGNAL FLAWS'
#                    else: odom.status = True
#                    if encSteer.q > encSteer.th :
#                        #print "encSteer.q", encSteer.q
#                        encSteer.status = False
#                        if encSteer.samples % 100 ==0:
#                            print 'ENC-STEER SIGNAL FLAWS'
#                    else: encSteer.status = True
#                    if imu.q > imu.th :
#                        #print "imu.q", imu.q
#                        imu.status = False
#                        if imu.samples % 100 ==0:
#                            print 'IMU SIGNAL FLAWS'
#                    else: imu.status = True
#                    if gps.q > gps.th :
#                        print "gps.q", gps.q
#                        #gps.status = False
#                        if gps.samples % 100 ==0:
#                            print 'GPS SIGNAL FLAWS'
#                    else: gps.status = True

                
                
                last_state = fukf1.x
                lastTimeGPS = currTime
                #odomx, odomy, odomvx, odomvy = 0, 0, 0, 0 # reset the internal encoder counters
"""
        #end--------------------------------------------------------#
        # UKF ------------------------------------------------------#
        #############################################################
#    break
#%%
print("Bags processed!\nSee plots!")
states_vector_1 = np.array(states_vector_1)
states_vector_2 = np.array(states_vector_2)
P_ = np.array(P_)
P_2 = np.array(P_2)
P_fukf = np.array(P_fukf)
posx = np.array(posx)
posy = np.array(posy)
steer = np.array(steer)
encoder = np.array(encoder)
vels = np.array(vels)
angles = np.array(angles)
anglesGPS = np.array(anglesGPS)
anglesacum = np.array(anglesacum)
beta_=np.array(beta_)
beta_2=np.array(beta_2)
odometryData = np.array(odometryData)
#fukfStates = np.array(fukf.states)
outputBatch2, covBatch2 = fukf2.batch_filter(states_vector_2)
#fukf1.hx = H_
#fukf1.P = np.zeros((6,6))
#fukf1.sigmas_h = np.zeros((13,6))
#outputBatch1, _ = fukf1.batch_filter(states_vector_1)
states_smooth, cov_smooth, _ = fukf2.rts_smoother(states_vector_2, P_2)

#%%
label_fusion_A=u'Fusi\xf3n-A'
label_fusion_B=u'Fusi\xf3n-B'
title_orientacion = u'Orientaci\xf3n [rad] - Tiempo [seg]'
plt.figure(1, tight_layout='True')
ax = plt.subplot(321)
plt.title('Trayectoria iCab - Coord. UTM')
plt.plot(posx, posy, 'b+',label='GPS', lw = 1) # data
#plt.plot(x_, y_, 'r-', label='UKF-gps',lw = 1) # filter
#plt.plot(outputBatch1[:, 0], outputBatch1[:, 1], 'k-',label=label_fusion_A, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.plot(outputBatch2[:, 0], outputBatch2[:, 1], 'k-',label=label_fusion_A, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
plt.plot(states_smooth[:, 0], states_smooth[:, 1], 'r-',label='smooth', lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
plt.plot(x_2, y_2, 'y-', label=label_fusion_B,lw = 1) # filter
plt.plot(+odometryData[:,0], +odometryData[:, 1], 'g-',label='Odometria', lw = 1) # Enc
plt.legend( loc='upper right', ncol=1, borderaxespad=0.3, shadow=True, fancybox=True)
ax.grid(clip_on=False, marker='+', markersize=1)
plt.axis('equal')


ax = plt.subplot(322)
plt.title(title_orientacion)
#plt.plot(timeImu, +angles[:,2], 'k-',label='ORIENTATION (IMU-quat)', lw = 1) # yaw data
plt.plot(timeImu, +anglesacum[:,2], 'g+',label='IMU', lw = 1) # data
plt.plot(timeGps, +anglesGPS[:,2], 'b+',label='GPS', lw = 1) # data
#plt.plot(timefukf1, 0*init_orientation+beta_[:], 'r-', label=label_fusion_A, lw = 1) # filter
#plt.plot(timefukf2, outputBatch2[:, 4], 'k-',label='batch_fukf2', lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
plt.plot(timefukf2, states_smooth[:, 4], 'r-',label='smooth', lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.plot(timefukf1, outputBatch1[:, 4], 'm-',label='batch_fukf2', lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
plt.plot(timefukf2, +beta_2[:], 'y-', label=label_fusion_B, lw = 1) # filter
plt.legend(loc='lower center', ncol=5, borderaxespad=0.3, shadow=True, fancybox=True)
ax.grid(clip_on=False, marker='+', markersize=1)

ax = plt.subplot(323)
plt.title('Coord X-UTM [m] - Tiempo [seg]')
plt.plot(timeGps, posx, 'b+',label='GPS', lw = 1) # gps
#plt.plot(timefukf1, x_, 'r-', label='UKF-gps',lw = 1) # filter
#plt.plot(timefukf2, outputBatch2[:, 0], 'k-',label=label_fusion_A, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
plt.plot(timefukf2, states_smooth[:, 0], 'r-',label='smooth', lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.plot(timefukf1, outputBatch1[:, 0], 'm-',label=label_fusion_A, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
plt.plot(timefukf2, x_2, 'y-', label=label_fusion_B,lw = 1) # filter
#plt.plot(timeEnc, odometryData[:,0], 'g-', label='ENC', lw=1 ) # enc
plt.legend(loc='upper right', ncol=1, borderaxespad=0.3, shadow=True, fancybox=True)
ax.grid(clip_on=False, marker='+', markersize=1)

ax = plt.subplot(324)
plt.title('Coord Y-UTM [m] - Tiempo [seg]')
plt.plot(timeGps, posy, 'b+',label='GPS', lw = 1) # data
#plt.plot(timefukf1, y_, 'r-', label='UKF-gps',lw = 1) # filter
#plt.plot(timefukf2, outputBatch2[:, 1], 'k-',label=label_fusion_A, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
plt.plot(timefukf2, states_smooth[:, 1], 'r-',label='smooth', lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.plot(timefukf1, outputBatch1[:, 1], 'm-',label=label_fusion_A, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
plt.plot(timefukf2, y_2, 'y-', label=label_fusion_B,lw = 1) # filter
#plt.plot(timeEnc, odometryData[:,1], 'g-', label='ENC', lw=1 ) # enc
plt.legend(loc='upper right', ncol=1, borderaxespad=0.3, shadow=True, fancybox=True)
ax.grid(clip_on=False, marker='+', markersize=1)

ax = plt.subplot(325)
plt.title('Componente x de la Velocidad [m/seg] - Tiempo [seg]')
plt.plot(timeGps, vels[:, 0], 'b+', label='GPS', lw = 1) # filter
#plt.plot(timefukf1, velx_, 'r-', label='UKF-gps', lw = 1) # filter
#plt.plot(timefukf2, outputBatch2[:, 2], 'k-',label=label_fusion_A, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
plt.plot(timefukf2, states_smooth[:, 2], 'r-',label='smooth', lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.plot(timefukf1, outputBatch1[:, 2], 'm-',label=label_fusion_A, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.plot(timeEnc, odometryData[:,2], 'g-p', label='ENC', lw=1 ) # enc
plt.plot(timefukf2, velx_2, 'y-', label=label_fusion_B, lw = 1) # filter
plt.legend(loc='lower center', ncol=4, borderaxespad=0.3, shadow=True, fancybox=True)
ax.grid(clip_on=False, marker='+', markersize=1)
#plt.axis('equal')

ax = plt.subplot(326)
plt.title('Componente y de la Velocidad [m/seg] - Tiempo [seg]')
plt.plot(timeGps, vels[:, 1], 'b+', label='GPS', lw = 1) # filter
#plt.plot(timefukf1, vely_, 'r-', label='UKF-gps', lw = 1) # filter
#plt.plot(timefukf2, outputBatch2[:, 3], 'k-',label=label_fusion_A, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
plt.plot(timefukf2, states_smooth[:, 3], 'r-',label='smooth', lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.plot(timefukf1, outputBatch1[:, 3], 'm-',label=label_fusion_A, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
plt.plot(timefukf2, vely_2, 'y-', label=label_fusion_B, lw = 1) # filter
#plt.plot(timeEnc, odometryData[:,3], 'g-p', label='ENC', lw=1 ) # enc
plt.legend(loc='lower center', ncol=4, borderaxespad=0.3, shadow=True, fancybox=True)
ax.grid(clip_on=False, marker='+', markersize=1)
#plt.axis('equal')

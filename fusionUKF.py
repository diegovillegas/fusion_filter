from math import tan, sin, cos
from filterpy.stats import plot_covariance_ellipse
from filterpy.kalman import MerweScaledSigmaPoints as SigmaPointsM
from filterpy.kalman import JulierSigmaPoints as SigmaPointsJ
from filterpy.kalman import SimplexSigmaPoints as SigmaPointsS
import numpy as np
from filterpy.kalman import unscented_transform
from filterpy.kalman import UnscentedKalmanFilter as UKF
import matplotlib.pyplot as plt
from filterpy.common import dot3
from scipy.linalg import inv
from decimal import Decimal
import glob, rosbag, math

'''
     state vector : x = [x, y, velx, vely, beta, steering_angle] : [m, m, m/s, m/s, gra, gra]
     measurement  : z_gps = [x , y, velx, vely] : [m, m, m/s, m/s]
     measurement  : z_imu = [wx , wy, wz] (roll, pitch, yaw) :[gra/s]X3
'''

UKF_PERIOD = 0.52#0.05157 # period of Kalman filter (seg)
FUKF2_PERIOD = 0.5
VAR_STEER = 15#0.087719298
wheelbase = 1.66
dt_GPS = 0.58 # 580 ms

#=================================================#

class sensor(object):
    def __init__( self, H_func, dim_z, dim_x, th):
        self.h_f   = H_func

        self.initValue = None
        self.value = None
        self.preValue = None
        self.acValue = None

        self.initTime = None
        self.time = None    # time when the recent measure has been read
        self.preTime = None # time when the previous measure was received

        self.rawdata = []
        self.offset = None
        self.R     = None
        self.meas_flg = False # either the sensor is being read or not
        self.samples  = 0
        self.dim_z = dim_z
        self._num_sigmas = 2*dim_x + 1
        self.sigmas_h = np.zeros((self._num_sigmas, self.dim_z))
        self.x_gain = np.zeros(dim_x)
        self.P_gain = np.zeros((dim_x, dim_x))
        self.q = None   # estadistico que sigue una distribucion ji-cuadrado
        self.S = None
        self.th = None  # threshold to decide if the sensor will be included in the fusion
        self.status = True # either the sensor passed the level required specified by self.th

    def read_data( self, data, R, time ):
        if self.samples==0:
            self.init_sensor( data, R, time)
        else:
            self.update_data( data, R, time )
        
    def init_sensor( self, data, R, time ):
        self.initTime = time
        self.initValue = data
        self.value = data
        self.acValue = self.initValue
        self.samples +=1
        print('Sensor started!\n')

    def update_data( self, data, R, time ):
        self.preTime = self.time
        self.preValue = self.value
        self.value = data
        #self.acValue += self.value
        self.time = time
        self.R    = R
        self.samples +=1

#=======================================================#

class FUKF(object):
    def __init__( self, dim_x, dt, fx,
                 points,
                 sqrt_fn=None, x_mean_fn=None, #z_mean_fn=None,
                 residual_x=None, residual_z=None ):
        self.init_flag = False
        self.states = []
        self.x = None
        self.P = None
        self.Q = None
        self.dim_x = dim_x
        self.prev_x = None
        self.x_p = np.zeros(self.dim_x)
        self.P_p = np.zeros((self.dim_x,self.dim_x))
        self._dt = dt
        self._num_sigmas = 2*self.dim_x + 1
        self.fx = fx
        self.x_mean = x_mean_fn
        self.points_fn = points
        # weights for the means and covariances.
        self.Wm, self.Wc = self.points_fn.weights()
        
        if sqrt_fn is None:
            self.msqrt = cholesky
        else:
            self.msqrt = sqrt_fn

        if residual_x is None:
            self.residual_x = np.subtract
        else:
            self.residual_x = residual_x

        if residual_z is None:
            self.residual_z = np.subtract
        else:
            self.residual_z = residual_z

        # sigma points transformed through f(x) and h(x)
        # variables for efficiency so we don't recreate every update
        self.sigmas_f = np.zeros((self._num_sigmas, self.dim_x))
    
    def predict( self, dt, fx_args=()):
        if not isinstance(fx_args, tuple):
            fx_args = (fx_args,)
        if dt is None:
            dt = self._dt

        UT = unscented_transform

        # calculate sigma points for given mean and covariance
        sigmas = self.points_fn.sigma_points( self.x, self.P )

        for i in range( self._num_sigmas ):
            self.sigmas_f[i] = self.fx( sigmas[i], dt , *fx_args )

        self.x_p, self.P_p = UT( self.sigmas_f, self.Wm, self.Wc, self.Q,
                            self.x_mean, self.residual_x )
        # normalize the components with angles?

    def calc_param( self, sensor, fx_args=() ):
        if not isinstance(fx_args, tuple):
            fx_args = (fx_args,)
        R = sensor.R
        z = sensor.value
        if z is None:
            return
        UT = unscented_transform
        if R is None:
            return
        elif np.isscalar( R ):
            R = np.eye( sensor.dim_z ) * R

        for i in range( sensor._num_sigmas ):
            sensor.sigmas_h[i] = sensor.h_f( self.sigmas_f[i], fx_args )
        # mean and covariance of prediction passed through unscented transform
        zp, Pz = UT( sensor.sigmas_h, self.Wm, self.Wc, R )
                    #z_mean, residual_z )
        # compute cross variance of the state and the measurements
        Pxz = np.zeros(( self.dim_x, sensor.dim_z ))
        for i in range(sensor._num_sigmas):
            dx = self.residual_x( self.sigmas_f[i], self.x_p )
            dz = self.residual_z( sensor.sigmas_h[i], zp )
            Pxz += self.Wc[i] * np.outer( dx, dz )
        K = np.dot( Pxz, inv(Pz) )   # Kalman gain
        y = self.residual_z(z, zp)   #residual
        # compute the q_i to define the validity domain of sensor
        sensor.q = np.dot( y.T , np.dot( inv( Pz ) , ( y )) )
        sensor.S = Pz
        x_gain = np.dot( K, y )
        P_gain = dot3( K, Pz, K.T )
        return ( x_gain, P_gain )

    def update_state( self, x_gains, P_gains ):
        (x_gain_GPS, x_gain_ENC, x_gain_ENCSTEER, x_gain_IMU) = x_gains
        (P_gain_GPS, P_gain_ENC, P_gain_ENCSTEER, P_gain_IMU) = P_gains
        #print ('GPS: {}  ENC: {}  IMU: {}'.format(x_gain_GPS,x_gain_ENC,x_gain_IMU))
        # updating the system state
        self.x = self.x_p + x_gain_GPS + x_gain_ENC + x_gain_ENCSTEER + x_gain_IMU
        self.P = inv( inv( self.P_p ) + ( P_gain_GPS + P_gain_ENC + P_gain_ENCSTEER + P_gain_IMU) )
        #self.x[4] = normalize_angle(self.x[4]) #normalize angles?
        self.prev_x = self.x

    def batch_filter(self, zs, Rs=None, UT=None):
        """ Performs the UKF filter over the list of measurement in `zs`.

        Parameters
        ----------

        zs : list-like
            list of measurements at each time step `self._dt` Missing
            measurements must be represented by 'None'.

        Rs : list-like, optional
            optional list of values to use for the measurement error
            covariance; a value of None in any position will cause the filter
            to use `self.R` for that time step.

        UT : function(sigmas, Wm, Wc, noise_cov), optional
            Optional function to compute the unscented transform for the sigma
            points passed through hx. Typically the default function will
            work - you can use x_mean_fn and z_mean_fn to alter the behavior
            of the unscented transform.

        Returns
        -------

        means: ndarray((n,dim_x,1))
            array of the state for each time step after the update. Each entry
            is an np.array. In other words `means[k,:]` is the state at step
            `k`.

        covariance: ndarray((n,dim_x,dim_x))
            array of the covariances for each time step after the update.
            In other words `covariance[k,:,:]` is the covariance at step `k`.
        """

        try:
            z = zs[0]
        except:
            assert not isscalar(zs), 'zs must be list-like'

        if self._dim_z == 1:
            assert isscalar(z) or (z.ndim==1 and len(z) == 1), \
            'zs must be a list of scalars or 1D, 1 element arrays'

        else:
            assert len(z) == self._dim_z, 'each element in zs must be a' \
            '1D array of length {}'.format(self._dim_z)

        z_n = np.size(zs, 0)
        if Rs is None:
            Rs = [None] * z_n

        # mean estimates from Kalman Filter
        if self.x.ndim == 1:
            means = zeros((z_n, self._dim_x))
        else:
            means = zeros((z_n, self._dim_x, 1))

        # state covariances from Kalman Filter
        covariances = zeros((z_n, self._dim_x, self._dim_x))

        for i, (z, r) in enumerate(zip(zs, Rs)):
            self.predict(UT=UT)
            self.update(z, r, UT=UT)
            means[i,:]         = self.x
            covariances[i,:,:] = self.P

        return (means, covariances)


    def rts_smoother(self, Xs, Ps, Qs=None, dt=None):
        """ Runs the Rauch-Tung-Striebal Kalman smoother on a set of
        means and covariances computed by the UKF. The usual input
        would come from the output of `batch_filter()`.

        Parameters
        ----------

        Xs : numpy.array
           array of the means (state variable x) of the output of a Kalman
           filter.

        Ps : numpy.array
            array of the covariances of the output of a kalman filter.

        Qs: list-like collection of numpy.array, optional
            Process noise of the Kalman filter at each time step. Optional,
            if not provided the filter's self.Q will be used

        dt : optional, float or array-like of float
            If provided, specifies the time step of each step of the filter.
            If float, then the same time step is used for all steps. If
            an array, then each element k contains the time  at step k.
            Units are seconds.

        Returns
        -------

        x : numpy.ndarray
           smoothed means

        P : numpy.ndarray
           smoothed state covariances

        K : numpy.ndarray
            smoother gain at each step

        Examples
        --------

        .. code-block:: Python

            zs = [t + random.randn()*4 for t in range (40)]

            (mu, cov, _, _) = kalman.batch_filter(zs)
            (x, P, K) = rts_smoother(mu, cov, fk.F, fk.Q)
        """

        assert len(Xs) == len(Ps)
        n, dim_x = Xs.shape

        if dt is None:
            dt = [self._dt] * n
        elif isscalar(dt):
            dt = [dt] * n

        if Qs is None:
            Qs = [self.Q] * n

        # smoother gain
        Ks = zeros((n,dim_x,dim_x))

        num_sigmas = self._num_sigmas

        xs, ps = Xs.copy(), Ps.copy()
        sigmas_f = zeros((num_sigmas, dim_x))

        for k in range(n-2,-1,-1):
            # create sigma points from state estimate, pass through state func
            sigmas = self.points_fn.sigma_points(xs[k], ps[k])
            for i in range(num_sigmas):
                sigmas_f[i] = self.fx(sigmas[i], dt[k])

            # compute backwards prior state and covariance
            xb = dot(self.Wm, sigmas_f)
            Pb = 0
            x = Xs[k]
            for i in range(num_sigmas):
                y = sigmas_f[i] - x
                Pb += self.Wm[i] * outer(y, y)
            Pb += Qs[k]

            # compute cross variance
            Pxb = 0
            for i in range(num_sigmas):
                z = sigmas[i] - Xs[k]
                y = sigmas_f[i] - xb
                Pxb += self.Wm[i] * outer(z, y)

            # compute gain
            K = dot(Pxb, inv(Pb))

            # update the smoothed estimates
            xs[k] += dot (K, xs[k+1] - xb)
            ps[k] += dot3(K, ps[k+1] - Pb, K.T)
            Ks[k] = K

        return (xs, ps, Ks)


#=======================================================#


def state_mean(sigmas, Wm):
    x = np.zeros(len(sigmas[0]))
    sum_sin_or = 0. #np.sum(np.dot(np.sin(sigmas[:,4]), Wm*0.01))
    sum_cos_or = 0. #np.sum(np.dot(np.cos(sigmas[:,4]), Wm*0.01))
    sum_sin_steer = 0. #np.sum(np.dot(np.sin(sigmas[:,5]), Wm))
    sum_cos_steer = 0. #np.sum(np.dot(np.cos(sigmas[:,5]), Wm))
    for i in range(len(sigmas)):
        s = sigmas[i]
        x[0] += s[0] * Wm[i] # x coord utm
        x[1] += s[1] * Wm[i]  # y coord utm
        x[2] += s[2] * Wm[i]  # vx
        x[3] += s[3] * Wm[i]  # vy
        sum_sin_or +=  np.sin(s[4]) * Wm[i]
        sum_cos_or +=  np.cos(s[4]) * Wm[i]
        sum_sin_steer +=  np.sin(s[5]) * Wm[i]
        sum_cos_steer +=  np.cos(s[5]) * Wm[i]
    x[4] = np.math.atan2(sum_sin_or, sum_cos_or)
    x[5] = np.math.atan2(sum_sin_steer, sum_cos_steer)
    return x

def z_mean(sigmas, Wm):
    x = np.zeros(4)
    #sum_sin = np.sum(np.dot(np.sin(sigmas[:, 2]), Wm))
    #sum_cos = np.sum(np.dot(np.cos(sigmas[:, 2]), Wm))
    x[0] = np.sum(np.dot(sigmas[:,0], Wm))
    x[1] = np.sum(np.dot(sigmas[:,1], Wm))
    x[2] = np.sum(np.dot(sigmas[:,2], Wm))
    x[3] = np.sum(np.dot(sigmas[:,3], Wm))
    #x[2] = np.math.atan2(sum_sin, sum_cos)
    return x

def quat2angle(quat):
    roll   = math.atan2(2*(quat.w*quat.x+quat.y*quat.z),1-2*(quat.x**2+quat.y**2))
    pitch   = math.asin(2*(quat.w*quat.y+quat.z*quat.x))
    yaw   = math.atan2(2*(quat.w*quat.z+quat.x*quat.y),1-2*(quat.y**2+quat.z**2))
    return (roll, pitch, yaw)

def decimals(x, n):
    return np.float(np.round(x,n))

def normalize_angle(x):
    x = np.mod(x, 2*np.pi)
    if x > np.pi:
        x -= 2*np.pi
    return x

def subtr_angles(a,b):
    return math.atan2( (math.sin(a)+math.sin(b)), (math.cos(a)+math.cos(b)))

def subtract_x(a, b):
    y = a - b
    #y[4] = subtr_angles(a[4], -b[4])
    #y[5] = subtr_angles(a[5], -b[5])
    return y

def subtract_x_2(a, b):
    y = a - b
    y[4] = normalize_angle(np.radians((a[4] -b[4])))
    #y[5] = subtr_angles(a[5], -b[5])
    return y

def H_(x, hx_args=()):
    return x

def H_gps(x, hx_args=()):
    return np.array([x[0],x[1],x[2], x[3]])

def H_imu(x, hx_args=()):
    return np.array([x[4]])

def H_encSteer(x, hx_args=()):
    return np.array([x[5]])

def H_odometry(x, hx_args ):
    """
        INPUT -> x = [x, y, vx, vy, orient, steer] 
        OUTPUT-> [newX, newY, newVelX, newVelY]
    """
    return np.array([ x[0], x[1], x[2], x[3] ])

def odometry( hdg, steering_angle, enc, dt):
    """
        INPUTS-> 
              -> hdg, steering_angle 
              -> enc = [ticks]
        OUTPUT-> [0, 0, newVelX, newVelY]
    """
    aux = Decimal(enc)/Decimal(13000)
    dist = float(aux)*1.4
    vel = dist*dt
    velx = vel*np.cos(hdg)/dt
    vely = vel*np.sin(hdg)/dt
    return np.array([0 , 0, velx, vely ])

def F__(x, dt):
    beta = x[4]
    vx = x[2]
    vy = x[3]
    steering_angle = x[5]
    distx = vx*dt
    disty = vy*dt
    dist=np.sqrt(distx**2+disty**2)
    vel=np.sqrt(vx**2+vy**2)
    # turning
    if abs(steering_angle) > 0.5:
        dx_ = (vel**2)*(dt**2)*tan(steering_angle)/(2*wheelbase)
        dy_ = np.sqrt(dist**2-dx_**2)
        Rot = np.array([[cos(beta), -sin(beta)],
                        [sin(beta),  cos(beta)]])
        result = np.cross(Rot, np.array([dx_, dy_]).T)
        dx_=result[0]
        dy_=result[1]
        r = wheelbase/tan(steering_angle)
        dbeta = normalize_angle(2*np.math.atan2(dist/2, r))
    # going straight forward
    else:
        dbeta = 0
        dx_ = dist*(math.sin(beta))
        dy_ = dist*(math.cos(beta))
    return x + np.array([dx_, dy_, 0, 0, dbeta, 0])

def F_(x, dt):
    hdg = x[4]
    vx = x[2]
    vy = x[3]
    steering_angle = x[5]
    distx = vx*dt
    disty = vy*dt
    dist=np.sqrt(distx**2+disty**2)
    if abs(steering_angle) > 0.5:
        beta = (dist/wheelbase)*np.tan(steering_angle)
        beta = normalize_angle(beta)
        #print("hdg ={}   |   beta ={}  \n".format(hdg, beta))
        # the icc's radius r
        r = wheelbase / np.tan(steering_angle)
        sinh, sinhb = np.sin(hdg), np.sin(hdg + beta)   
        cosh, coshb = np.cos(hdg), np.cos(hdg + beta)
        x += np.array([-r*sinh + r*sinhb, r*cosh - r*coshb, 0, 0, np.radians(beta), 0])
        return x
    else:
        return x + np.array([distx, disty, 0, 0, 0, 0])

def F___(x, dt):
    hdg = x[4]
    vx = x[2]
    vy = x[3]
    steering_angle = x[5]
    distx = vx*dt
    disty = vy*dt
    dist=np.sqrt(distx**2+disty**2)
    if abs(steering_angle) > 0.01:
        beta = (dist/wheelbase)*np.tan(steering_angle)
        #beta = normalize_angle(beta)
        #print("hdg ={}   |   beta ={}  \n".format(hdg, beta))
        # the icc's radius r
        r = wheelbase / np.tan(steering_angle)
        sinh, sinhb = np.sin(hdg), np.sin(hdg + beta)   
        cosh, coshb = np.cos(hdg), np.cos(hdg + beta)
        x += np.array([-r*sinh + r*sinhb, r*cosh - r*coshb, 0, 0, (beta), 0])
        return x
    else:
        return x + np.array([distx, disty, 0, 0, 0, 0])


#=======================================================#


#def main():

#Variables Flags y dataStorage.
x_, y_, beta_, dts = [], [], [], []
x_2, y_2, beta_2 = [], [], []
X_, P_, X_2, P_fukf2, P_fukf, States_fukf2 = [], [], [], [], [], []
timeukf = []
timefukf2 = []
timeGps = []
timeImu = []
timeEnc = []
steer_, velx_, vely_ = [], [], []
steer_2, velx_2, vely_2 = [], [], []
last_state, curr_steer, prev_steer = None, None, None
curr_ticks, prev_ticks = None, None
acumbx, acumby, acumbz = 0., 0., 0.
steer, vels = [], []
encoder, odometryData = [], []
odomx, odomy, odomvx, odomvy = 0, 0, 0, 0
angles, anglesacum , anglesGPS = [], [], []
posx, posy = [] , []
flagInit, imuInit=False, False
flagInitGPS = False
flagInitUKF = False
init_pos, vel_x, vel_y, init_orientation = None, None, None, None

process_points = SigmaPointsM(n=6, alpha=0.99, beta=2, kappa=-1) #SigmaPoints for the process model. J(n=6, kappa=0.05)
#process_points_fukf = SigmaPointsM(n=6, alpha=0.001, beta=2, kappa=-1) #SigmaPoints for the process model.
process_points_fukf = SigmaPointsJ(n=6, kappa=0.001)#alpha=1, beta=2, kappa=-0.5) #SigmaPoints for the process model.
process_points_fukf2 = SigmaPointsJ(n=6, kappa=0.001)#alpha=1, beta=2, kappa=-0.5) #SigmaPoints for the process model.

#ang_points.sigma_points(x=np.array([2.43, 0.5]), P=np.diag((np.pi/2, 10)))
#process_points_fukf.sigma_points(x=np.array([100, 100, 2.5, 2.5, 2.43, 0]), P = np.diag([1000, 1000, 10, 10, 5, 5]))


# Define UKF ***********
ukf = UKF( dim_x=6, dim_z=4, fx=F_, hx=H_gps, dt=None, points=process_points)
ukf.P = np.diag([1000, 1000, 10, 10, 5, 5])
ukf.Q = np.diag((1.1, 1.1, 5, 5, 1, 0.1))
         
#********** Define DECOUPLED UKF-FUSION METHOD MULTI-SENSOR (GPS-IMU-ENCODER) ***********
fukf = FUKF( dim_x=6, dt = UKF_PERIOD, fx=F_, points=process_points_fukf, residual_x=subtract_x )
fukf.P = np.diag([100, 100, 10, 10, 5, 5])
fukf.Q = np.diag((1, 1, .5, .5, .8, 2))

#********** Define CENTRALIZED UKF-FUSION METHOD (GPS-IMU) ***********
fukf2 = UKF( dim_x=6, dim_z=6, fx=F_, hx=H_, dt=None, points=process_points_fukf2)
fukf2.P = np.diag([100, 100, 10, 10, 5, 5])
fukf2.Q = np.diag((1.1, 1.1, .5, .5, .8, .1))

# init the sensors
gps = sensor(H_gps, dim_z=4, dim_x=fukf.dim_x, th=3)
odom = sensor(H_odometry, dim_z=4, dim_x=fukf.dim_x, th=2)
encSteer = sensor(H_encSteer, dim_z=1, dim_x=fukf.dim_x, th=2)
imu = sensor(H_imu, dim_z=1, dim_x=fukf.dim_x, th=2)

#Listar archivos fuente
path_bagfile = '/media/diego/DataPart2/UC3M/2doCuatrimestre/Trabajo_LSI/data_icab/'
velofiles = glob.glob(path_bagfile + 'velo_test_1*.bag')
mapfiles  = glob.glob(path_bagfile + 'map_test*.bag')
velofiles.sort()
mapfiles.sort()
init_time_ = 1474459502.06
#reading the bagfile
for bagfile in velofiles[:]:
    bagfile = velofiles[15]
    # select the rosbagfile
    bag = rosbag.Bag((bagfile))
    print 'Procesando archivo {} .'.format(bagfile)
    # rostopics of interest depending on the bag file version
    if bagfile in velofiles:
        topic_POS2 = '/icab1/mavros/global_position/global' # type: nav_msgs/Odometry
        topic_POS = '/icab1/mavros/global_position/local' # type: nav_msgs/Odometry
        topic_ENC = '/icab1/movement_manager/status_info' # type: icab_msgs/StatusInfo
        topic_IMU = '/icab1/mavros/imu/data'              # type: sensor_msgs/Imu
    else:
        topic_POS2 ='/mavros/global_position/global' # type: nav_msgs/Odometry
        topic_POS = '/mavros/global_position/local' # type: nav_msgs/Odometry
        topic_ENC = '/movement_manager/status_info' # type: icab_msgs/StatusInfo
        topic_IMU = '/mavros/imu/data'              # type: sensor_msgs/Imu
    for topic_, msg_, t_ in bag.read_messages():
        if flagInit==False:
            lastTime = t_.to_sec()
            flagInit = True
            continue
#        if t_.to_sec() < lastTime+27: continue
        if topic_ == topic_POS:
            lastTimeGPS = t_.to_sec()
            init_pos = [msg_.pose.pose.position.x.real, msg_.pose.pose.position.y.real]
            vel_x = msg_.twist.twist.linear.x
            vel_y = msg_.twist.twist.linear.y
            if gps.samples ==0:
                __, __, init_orientation = quat2angle(msg_.pose.pose.orientation)
                dataOdom = np.array([float(init_pos[0]), float(init_pos[1]), vel_x, vel_y])
            hx, hy, hz = quat2angle(msg_.pose.pose.orientation)
            currTime = t_.to_sec()
            pos_x = msg_.pose.pose.position.x.real
            pos_y = msg_.pose.pose.position.y.real
            vel_x = msg_.twist.twist.linear.x
            vel_y = msg_.twist.twist.linear.y
            var_x = msg_.pose.covariance[0] # error en x
            var_y = msg_.pose.covariance[7] # error en y
            v_o = msg_.pose.covariance[35]
            R = np.diag((var_x, var_y, 2, 2))
            posx.append(pos_x)
            posy.append(pos_y)
            vels.append(( msg_.twist.twist.linear.x, msg_.twist.twist.linear.y, msg_.twist.twist.linear.z ))
            anglesGPS.append((hx, hy, hz))
            timeGps.append(t_.to_sec())
            
            gps.read_data(data=np.array([pos_x, pos_y, vel_x, vel_y]), R=R, time=t_.to_sec())
        # ---------ENCODERS-----------------------------------------#
        if topic_ == topic_ENC and imu.samples>0:
            if prev_steer is not None:
                curr_steer = msg_.dir.angle.real
                curr_ticks = msg_.trc.val_encoder.real
                delta_steer = curr_steer - prev_steer
                delta_ticks = curr_ticks - prev_ticks
                delta_t     = t_.to_sec() - prev_time_enc
                steer.append(msg_.dir.angle.real-prev_steer)
                timeEnc.append(t_.to_sec())
                encoder.append(msg_.trc.val_encoder.real)
                encSteer.read_data(data=delta_steer, R=VAR_STEER*1.5, time=t_.to_sec())
                #print 'delta_ticks {}'.format(delta_ticks)
                nDataOdom = odometry( hdg=imu.value, steering_angle=encSteer.value, enc=delta_ticks, dt=delta_t )
                dataOdom[0] += nDataOdom[0]
                dataOdom[1] += nDataOdom[1]
                dataOdom[2] = nDataOdom[2]
                dataOdom[3] = nDataOdom[3]
                #print("dataOdom : {}  \n time {} \n dsteer {} \n dticks {}\n".format(dataOdom, delta_t, delta_steer, delta_ticks))
                odomx, odomy, odomvx, odomvy = dataOdom
                odometryData.append((odomx, odomy, odomvx, odomvy))
                odom.read_data(data=dataOdom, R=np.diag((500, 500, 100, 100)), time=t_.to_sec())
                prev_steer = curr_steer
                prev_ticks = curr_ticks
                prev_time_enc = t_.to_sec()
            else:
                prev_steer = msg_.dir.angle.real
                prev_ticks = msg_.trc.val_encoder.real
                prev_time_enc = t_.to_sec()
        # ---------IMU----------------------------------------------#
        if topic_ == topic_IMU  and gps.samples>0:
            if imuInit==False:
                prevTimeIMU = t_.to_sec()
                acumbz = init_orientation
                imuInit = True
            else:
                dt = t_.to_sec() - prevTimeIMU
                b_x, b_y, b_z = quat2angle( msg_.orientation )
                acumbx += (dt*msg_.angular_velocity.x)
                acumby += (dt*msg_.angular_velocity.y)
                acumbz += (dt*msg_.angular_velocity.z)
                anglesacum.append((acumbx, acumby, acumbz))
                angles.append((+b_x, +b_y, +b_z))
                imu.read_data(data=acumbz, R=10, time=t_.to_sec())
                timeImu.append(t_.to_sec())
                prevTimeIMU = t_.to_sec()
        #############################################################
        #init-------------------------------------------------------#
        # UKF ------------------------------------------------------#
        if init_pos is not None and vel_x is not None and vel_y is not None and init_orientation is not None and not flagInitUKF and gps.samples>2 and encSteer.samples>2 and imu.samples>2 and odom.samples>2:
            #START THE UKF#
            x = np.array([init_pos[0], init_pos[1],  vel_x, vel_y, init_orientation, 0])
            print("INIT STATE X:{} \n".format(x))
            ukf.x = np.array([init_pos[0], init_pos[1],  vel_x, vel_y, init_orientation*0, 0])
            print("UKF-(GPS) STARTED!")
            #START THE Fusion-UKF#
            time_fukf = t_.to_sec()
            fukf.x = x
            print("FUKF 1 STARTED!")
            #START THE Fusion-UKF2#
            time_fukf2 = t_.to_sec()
            fukf2.x = x
            print("FUKF 2 STARTED!")
            flagInitUKF = True
        if flagInitUKF:
            if (Decimal(t_.to_sec()) - Decimal(time_fukf2) >= FUKF2_PERIOD):
                #==================#
                #-----PREDICT------#
                #==================#
                fukf2.predict(dt =t_.to_sec()-time_fukf2 )#UKF_PERIOD) # FUSION FILTER : METHOD B
                time_fukf2 = t_.to_sec()

                #==================#
                #-----UPDATE-------#
                #==================#
                fukf2.update(np.array([pos_x, pos_y, vel_x, vel_y, acumbz, delta_steer]), R= np.diag((var_x, var_y, 2, 2, 3, VAR_STEER)), hx_args=())
                #print var_x, var_y, imu.q

                
                timefukf2.append(t_.to_sec())
                x_2.append(fukf2.x[0])
                y_2.append(fukf2.x[1])
                beta_2.append( fukf2.x[4])
                velx_2.append(fukf2.x[2])
                vely_2.append(fukf2.x[3])
                steer_2.append(fukf2.x[5])
                P_fukf2.append(fukf2.P)
                States_fukf2.append(fukf2.x)
                
            if (Decimal(t_.to_sec()) - Decimal(time_fukf) >= UKF_PERIOD):
                #==================#
                #-----PREDICT------#
                #==================#
                ukf.predict(dt = UKF_PERIOD)
                dts.append(currTime - lastTimeGPS)
                fukf.predict(dt=t_.to_sec()-time_fukf)#UKF_PERIOD)
                time_fukf = t_.to_sec()

                #==================#
                #-----UPDATE-------#
                #==================#
                ukf.update([pos_x, pos_y, vel_x, vel_y], R= R, hx_args=())
                ( gps.x_gain, gps.P_gain ) = fukf.calc_param( gps )
                ( encSteer.x_gain, encSteer.P_gain ) = fukf.calc_param( encSteer )
                ( imu.x_gain, imu.P_gain ) = fukf.calc_param( imu )
                ( odom.x_gain, odom.P_gain ) = fukf.calc_param( odom )
                fukf.update_state([gps.x_gain*gps.status, odom.x_gain*odom.status, encSteer.x_gain*encSteer.status, imu.x_gain*imu.status],[gps.P_gain*gps.status, odom.P_gain*odom.status, encSteer.P_gain*encSteer.status, imu.P_gain*imu.status])
                if last_state is not None:
                    timeukf.append(t_.to_sec())
                    x_.append(ukf.x[0])
                    y_.append(ukf.x[1])
                    beta_.append((ukf.x[4]))
                    velx_.append(ukf.x[2])
                    vely_.append(ukf.x[3])
                    steer_.append(ukf.x[5])
                    P_.append(ukf.P.diagonal())
                    P_fukf.append(fukf.P)
                    fukf.states.append(fukf.x)
                last_state = ukf.x
                lastTimeGPS = currTime
                #odomx, odomy, odomvx, odomvy = 0, 0, 0, 0 # reset the internal encoder counters
        #end--------------------------------------------------------#
        # UKF ------------------------------------------------------#
        #############################################################
    break
#%%
P_ = np.array(P_)
P_fukf = np.array(P_fukf)
P_fukf2 = np.array(P_fukf2)
posx = np.array(posx)
posy = np.array(posy)
steer = np.array(steer)
encoder = np.array(encoder)
vels = np.array(vels)
angles = np.array(angles)
anglesGPS = np.array(anglesGPS)
anglesacum = np.array(anglesacum)
beta_=np.array(beta_)
beta_2=np.array(beta_2)
odometryData = np.array(odometryData)
#%%
States_fukf = np.array(fukf.states)
states_smooth_desac, cov_smooth_desac, _ = fukf.rts_smoother(States_fukf, P_fukf, dt=0.5)

States_fukf2 = np.array(States_fukf2)
P_fukf2 = np.array(P_fukf2)
states_smooth_centr, cov_smooth2_centr, _ = fukf2.rts_smoother(States_fukf2, P_fukf2, dt=0.5)

#%%
label_fusion_A=u'Desacoplado'
label_smooth_fusion_A =u'+Suavizado Desac.' # smooth of desacoplado
label_fusion_B=u'Centralizado'
label_smooth_fusion_B =u'+Suavizado Cent.' # smooth of desacoplado
plt.figure(1, tight_layout='True')
ax = plt.subplot(321)
plt.title('(a) Trayectoria en coordenadas UTM', y=-0.3)
plt.plot(posx, posy, 'b+',label='GPS', lw = 1) # UTM coordinates from GPS
plt.plot(x_, y_, 'g-', label='UKF-gps',lw = 1) # UKF filter monosensor (GPS)
plt.plot(States_fukf[:, 0], States_fukf[:, 1], 'k-',label=label_fusion_A, lw = 1) # ukf decoupled fusion GPS-IMU-ENCODER_STEER
plt.plot(states_smooth_desac[:, 0], states_smooth_desac[:, 1], 'y-',label=label_smooth_fusion_A, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
plt.plot(x_2, y_2, 'r-', label=label_fusion_B, lw = 1) # filter
plt.plot(states_smooth_centr[:, 0], states_smooth_centr[:, 1], 'c-',label=label_smooth_fusion_B, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.plot(+odometryData[:,0], +odometryData[:, 1], 'g-',label='Odometria', lw = 1) # Enc
plt.legend( loc='upper right', ncol=1, borderaxespad=0.3, shadow=True, fancybox=True, fontsize=10)
ax.grid(clip_on=False, marker='+', markersize=1)
plt.axis('equal')
plt.ylabel('Y UTM (m)')
plt.xlabel('X UTM (m)')

ax = plt.subplot(322)
title_b = u'(b) Rotaci\xf3n acumulada vs tiempo de navegaci\xf3n'
ylabel = u'Rotaci\xf3n acumulada (radianes)'
plt.title(title_b, y=-0.3)
#plt.plot(timeImu, +angles[:,2], 'k-',label='ORIENTATION (IMU-quat)', lw = 1) # yaw data
plt.plot(timeImu, +anglesacum[:,2], 'b+',label='IMU', lw = 1) # data
#plt.plot(timeGps, +anglesGPS[:,2], 'b+',label='GPS', lw = 1) # data
#plt.plot(timeukf, init_orientation+beta_[:], 'g-', label='UKF-gps', lw = 1) # filter
plt.plot(timeukf, States_fukf[:, 4], 'k-',label=label_fusion_A, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
plt.plot(timeukf, states_smooth_desac[:, 4], 'y-',label=label_smooth_fusion_A, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
plt.plot(timefukf2, +beta_2[:], 'r-', label=label_fusion_B, lw = 1) # filter
plt.plot(timefukf2, states_smooth_centr[:, 4], 'c-',label=label_smooth_fusion_B, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
plt.legend(loc='upper right', ncol=1, borderaxespad=0.3, shadow=True, fancybox=True, fontsize=10)
#plt.legend(loc='lower right', ncol=1, borderaxespad=0.3, shadow=True, fancybox=True, fontsize=10)
ax.grid(clip_on=False, marker='+', markersize=1)
plt.ylabel(ylabel)
plt.xlabel('Tiempo (s)')

ax = plt.subplot(323)
title_c = u'(c) Coordenada X UTM global vs tiempo de navegaci\xf3n' 
plt.title(title_c, y=-0.3)
plt.plot(timeGps, posx, 'b+',label='GPS', lw = 1) # gps
plt.plot(timeukf, x_, 'g-', label='UKF-gps',lw = 1) # filter
plt.plot(timeukf, States_fukf[:, 0], 'k-',label=label_fusion_A, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
plt.plot(timeukf, states_smooth_desac[:, 0], 'y-',label=label_smooth_fusion_A, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
plt.plot(timefukf2, x_2, 'r-', label=label_fusion_B, lw = 1) # filter
plt.plot(timefukf2, states_smooth_centr[:, 0], 'c-',label=label_smooth_fusion_B, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.plot(timeEnc, odometryData[:,0], 'g-', label='ENC', lw=1 ) # enc
#plt.legend(loc='lower center', ncol=3, borderaxespad=0.3, shadow=True, fancybox=True)
ax.grid(clip_on=False, marker='+', markersize=1)
plt.ylabel('X UTM (m)')
plt.xlabel('Tiempo (s)')

ax = plt.subplot(324)
title_d = u'(d) Coordenada Y UTM global vs tiempo de navegaci\xf3n' 
plt.title(title_d, y=-0.3)
plt.plot(timeGps, posy, 'b+',label='GPS', lw = 1) # data
plt.plot(timeukf, y_, 'g-', label='UKF-gps',lw = 1) # filter
plt.plot(timeukf, States_fukf[:, 1], 'k-',label=label_fusion_A, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
plt.plot(timeukf, states_smooth_desac[:, 1], 'y-',label=label_smooth_fusion_A, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
plt.plot(timefukf2, y_2, 'r-', label=label_fusion_B,lw = 1) # filter
plt.plot(timefukf2, states_smooth_centr[:, 1], 'c-',label=label_smooth_fusion_B, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.plot(timeEnc, odometryData[:,1], 'g-', label='ENC', lw=1 ) # enc
plt.legend(loc='lower right', ncol=1, borderaxespad=0.3, shadow=True, fancybox=True, fontsize=10)
ax.grid(clip_on=False, marker='+', markersize=1)
plt.ylabel('Y UTM (m)')
plt.xlabel('Tiempo (seg)')

ax = plt.subplot(325)
title_e = u'(e) Coordenada X Velocidad vs tiempo de navegaci\xf3n' 
plt.title(title_e, y=-0.3)
plt.plot(timeGps, vels[:, 0], 'b+', label='GPS', lw = 1) # filter
plt.plot(timeukf, velx_, 'g-', label='UKF-gps', lw = 1) # filter
plt.plot(timeukf, States_fukf[:, 2], 'k-',label=label_fusion_A, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.plot(timeEnc, odometryData[:,2], 'g-p', label='ENC', lw=1 ) # enc
plt.plot(timeukf, states_smooth_desac[:, 2], 'y-',label=label_smooth_fusion_A, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
plt.plot(timefukf2, velx_2, 'r-', label=label_fusion_B, lw = 1) # filter
plt.plot(timefukf2, states_smooth_centr[:, 2], 'c-',label=label_smooth_fusion_B, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.legend(loc='lower center', ncol=3, borderaxespad=0.3, shadow=True, fancybox=True)
ax.grid(clip_on=False, marker='+', markersize=1)
plt.ylabel('Velocidad en x (m/s)')
plt.xlabel('Tiempo (s)')

ax = plt.subplot(326)
title_f = u'(f) Coordenada Y Velocidad vs tiempo de navegaci\xf3n' 
plt.title(title_f, y=-0.3)
plt.plot(timeGps, vels[:, 1], 'b+', label='GPS', lw = 1)
plt.plot(timeukf, vely_, 'g-', label='UKF-gps', lw = 1)
plt.plot(timeukf, States_fukf[:, 3], 'k-',label=label_fusion_A, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
plt.plot(timeukf, states_smooth_desac[:, 3], 'y-',label=label_smooth_fusion_A, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
plt.plot(timefukf2, vely_2, 'r-', label=label_fusion_B, lw = 1) # filter
plt.plot(timefukf2, states_smooth_centr[:, 3], 'c-',label=label_smooth_fusion_B, lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.plot(timeEnc, odometryData[:,3], 'g-p', label='ENC', lw=1 ) # enc
plt.legend(loc='lower right', ncol=1, borderaxespad=0.3, shadow=True, fancybox=True, fontsize=10)
#plt.legend(loc='upper right', ncol=1, borderaxespad=0.3, shadow=True, fancybox=True, fontsize=10)
ax.grid(clip_on=False, marker='+', markersize=1)
plt.ylabel('Velocidad en y (m/s)')
plt.xlabel('Tiempo (s)')

#%%

#plt.figure(2)
#ax = plt.subplot(231)
#plt.title('Trayectoria iCab - Coordenadas UTM')
#plt.plot(posx, posy, 'b+',label='GPS', lw = 1) # data
#plt.plot(fukfStates[:, 0], fukfStates[:, 1], 'k-',label='UKF-Fusion-A', lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
##plt.plot(odometryData[:,0], odometryData[:, 1], 'g+',label='ODOM', lw = 1) # data
#plt.plot(x_, y_, 'r-', label='UKF-simple (GPS)',lw = 1) # filter
#plt.plot(x_2, y_2, 'y-', label='UKF-Fusion-B',lw = 1) # filter
#plt.legend(loc='upper right')
#ax.grid(clip_on=False, marker='+', markersize=1)
#plt.axis('equal')
#
#ax = plt.subplot(232)
#plt.title('Orientacion acumulada (rad) vs Tiempo (seg)')
##plt.plot(timeImu, +angles[:,2], 'k-',label='ORIENTATION (IMU-quat)', lw = 1) # yaw data
##plt.plot(timeImu, +init_orientation+anglesacum[:,2], 'g-',label='ORIENTATION (IMU-angularveloc)', lw = 1) # data
#plt.plot(timeGps, +anglesGPS[:,2], 'b-',label='GPS', lw = 1) # data
#plt.plot(timeukf, fukfStates[:, 4], 'k-',label='UKF-Fusion-A', lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.plot(timeukf, +beta_[:], 'r-', label='UKF-simple (GPS)', lw = 1) # filter
#plt.plot(timefukf2, +beta_2[:], 'y-', label='UKF-Fusion-B', lw = 1) # filter
#plt.legend(loc='upper left')
#ax.grid(clip_on=False, marker='+', markersize=1)
#
#ax = plt.subplot(233)
#plt.title('Angulo de direccion (rad) vs Tiempo (seg)')
#plt.plot(timeEnc, steer, 'b-',label='ENCODER', lw = 1) # data
#plt.plot(timeukf, fukfStates[:, 5], 'k-',label='UKF-Fusion-A', lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.plot(timeukf, steer_, 'r-', label='UKF-simple (GPS)', lw = 1) # filter
#plt.plot(timefukf2, steer_2, 'y-', label='UKF-Fusion-B', lw = 1) # filter
#plt.legend(loc='upper left')
#ax.grid(clip_on=False, marker='+', markersize=1)


#%%
#plt.figure(3)    
#ax = plt.subplot(221)
#plt.title('Error States X coordinate')
#plt.plot(timeukf, P_[:, 0], 'r-',label='UKF-simple (GPS)', lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.plot(timefukf2, P_2[:, 0], 'y-',label='UKF-Fusion-B', lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.plot(timeukf, P_fukf[:, 0], 'k-',label='UKF-Fusion-A', lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.legend(loc='upper right')
#ax.grid(clip_on=False, marker='+', markersize=1)
#
#ax = plt.subplot(222)
#plt.title('Error States Y coordinate')
#plt.plot(timeukf, P_[:, 1], 'r-',label='UKF-simple (GPS)', lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.plot(timefukf2, P_2[:, 1], 'y-',label='UKF-Fusion-B', lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.plot(timeukf, P_fukf[:, 1], 'k-',label='UKF-Fusion-A', lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.legend(loc='upper right')
#ax.grid(clip_on=False, marker='+', markersize=1)
#
#ax = plt.subplot(223)
#plt.title('Error States Orientation')
#plt.plot(timeukf, P_[:, 4], 'r-',label='UKF-simple (GPS)', lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.plot(timefukf2, P_2[:, 4], 'y-',label='UKF-Fusion-B', lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.plot(timeukf, P_fukf[:, 4], 'k-',label='UKF-Fusion-A', lw = 1) # ukf fusion GPS-IMU-ODOMETRIA
#plt.legend(loc='upper right')
#ax.grid(clip_on=False, marker='+', markersize=1)


#main()
# README #

Download the following files: fusion_filter.py - ukf_functions.py - ukf_classes.py - constants.py, to read from a bagfiles where
there are rostopics from the autonomous car iCAB2 of the Carlos III Univesity of Madrid. Also is possible to read on a real-time
ros environment. 

### What is this repository for? ###

To apply/learn sensor fusion techniques using adaptive filtering methods, based on filterpy library, thanks to Roger Labbe.

A possible future work could be adding visual odometry's techniques for the fusion process in kalman filtering. 

### Who do I talk to? ###

You can email me for any questions to diegoavillegasg@gmail.com

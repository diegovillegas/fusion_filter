from math import tan, sin, cos
from filterpy.stats import plot_covariance_ellipse
from filterpy.kalman import MerweScaledSigmaPoints as SigmaPoints
import numpy as np
from filterpy.kalman import unscented_transform
from filterpy.kalman import UnscentedKalmanFilter as UKF
import matplotlib.pyplot as plt
import pickle, glob, rosbag, math


'''
     state vector : x = [x, y, velx, vely, beta, steering_angle] : [m, m, m/s, m/s, gra, gra]
     measurement  : z_gps = [x , y, velx, vely] : [m, m, m/s, m/s]
     measurement  : z_imu = [wx , wy, wz] (roll, pitch, yaw) :[gra/s]X3
'''
global UKF_PERIOD
UKF_PERIOD = 0.05157#0.05157 # period of Kalman filter (seg)
VAR_STEER = 0.087719298

def state_mean(sigmas, Wm):
    x = np.zeros(len(sigmas[0]))
    sum_sin_or = np.sum(np.dot(np.sin(sigmas[:,4]), Wm))
    sum_cos_or = np.sum(np.dot(np.cos(sigmas[:,4]), Wm))
    sum_sin_steer = np.sum(np.dot(np.sin(sigmas[:,5]), Wm))
    sum_cos_steer = np.sum(np.dot(np.cos(sigmas[:,5]), Wm))
    x[0] = np.sum(np.dot(sigmas[:,0], Wm)) # x coord utm
    x[1] = np.sum(np.dot(sigmas[:,1], Wm)) # y coord utm
    x[2] = np.sum(np.dot(sigmas[:,2], Wm)) # vx
    x[3] = np.sum(np.dot(sigmas[:,3], Wm)) # vy
    x[4] = np.math.atan2(sum_sin_or, sum_cos_or)
    x[5] = np.math.atan2(sum_sin_steer, sum_cos_steer)
    return x

def z_mean(sigmas, Wm):
    x = np.zeros(4)
    #sum_sin = np.sum(np.dot(np.sin(sigmas[:, 2]), Wm))
    #sum_cos = np.sum(np.dot(np.cos(sigmas[:, 2]), Wm))
    x[0] = np.sum(np.dot(sigmas[:,0], Wm))
    x[1] = np.sum(np.dot(sigmas[:,1], Wm))
    x[2] = np.sum(np.dot(sigmas[:,2], Wm))
    x[3] = np.sum(np.dot(sigmas[:,3], Wm))
    #x[2] = np.math.atan2(sum_sin, sum_cos)
    return x

def quat2angle(quat):
    roll   = math.atan2(2*(quat.w*quat.x+quat.y*quat.z),1-2*(quat.x**2+quat.y**2))
    pitch   = math.asin(2*(quat.w*quat.y+quat.z*quat.x))
    yaw   = math.atan2(2*(quat.w*quat.z+quat.x*quat.y),1-2*(quat.y**2+quat.z**2))
    return (roll, pitch, yaw)

def decimals(x, n):
    return np.float(np.round(x,n))

def normalize_angle(x):
    x = np.mod(x, 2*np.pi)
    if x > np.pi:
        x -= 2*np.pi
    return x

def subtr_angles(a,b):
    return math.atan2( (math.sin(a)+math.sin(b)), (math.cos(a)+math.cos(b)))

def subtract_x(a, b):
    y = a - b
    y[4] = subtr_angles(a[4], -b[4])
    y[5] = subtr_angles(a[5], -b[5])
    return y

def H_(x):
    return np.array([x[0],x[1],x[2], x[3]])

def F__(x, dt):
    beta = x[4]
    vx = x[2]
    vy = x[3]
    steering_angle = x[5]
    distx = vx*dt_GPS
    disty = vy*dt
    # turning
    if abs(steering_angle) > 0.01:
        dx_ = (vel**2)*(dt**2)*tan(steering_angle)/(2*wheelbase)
        dy_ = np.sqrt(dist**2-dx_**2)
        Rot = np.array([[cos(beta), -sin(beta)],
                        [sin(beta),  cos(beta)]])
        result = np.cross(Rot, np.array([dx_, dy_]).T)
        dx_=result[0]
        dy_=result[1]
        r = wheelbase/tan(steering_angle)
        dbeta = 2*np.math.atan2(dist/2, r)
    # going straight forward
    else:
        dbeta = 0
        dx_ = dist*sin(beta)
        dy_ = dist*cos(beta)
    return x + np.array([dx_, dy_, 0, 0, 0])

def F_(x, dt):
    hdg = x[4]
    vx = x[2]
    vy = x[3]
    steering_angle = x[5]
    distx = vx*dt
    disty = vy*dt
    dist=np.sqrt(distx**2+disty**2)
    if abs(np.degrees(steering_angle)) > 0.5:
        beta = (dist/wheelbase)*np.tan(steering_angle)
        # the icc's radius r
        r = wheelbase / np.tan(steering_angle)
        sinh, sinhb = np.sin(hdg), np.sin(hdg + beta)   
        cosh, coshb = np.cos(hdg), np.cos(hdg + beta)
        x += np.array([-r*sinh + r*sinhb, r*cosh - r*coshb, 0, 0, beta, 0])
        return x
    else:
        return x + np.array([distx, disty, 0, 0, 0, 0])

def odometry(x, enc, dt): #INPUT = [x, y, vx, vy, orient, steer] OUTPUT = [newX, newY, newVelX, newVelY]
    hdg = x[4]
    steering_angle = x[5]
    dist = enc/13000*1.4
    distx = dist*np.cos(hdg)
    disty = dist*np.sin(hdg)
    velx = distx/dt
    vely = disty/dt
    if abs(np.degrees(steering_angle)) > 0.5:
        beta = (dist/wheelbase)*np.tan(steering_angle)
        r = wheelbase / np.tan(steering_angle)
        sinh, sinhb = np.sin(hdg), np.sin(hdg + beta)
        cosh, coshb = np.cos(hdg), np.cos(hdg + beta)
        return np.array([x[0]-r*sinh + r*sinhb, x[1]+r*cosh - r*coshb, x[2] + velx, x[3] + vely ])
    else:
        return np.array([x[0] + distx, x[1] + disty, x[2], x[3]])

wheelbase = 1.66
x_, y_, beta_, cov, dts = [], [], [], [], []
X_, P_ = [], []
timeukf = []
timeGps = []
timeImu = []
timeEnc = []
steer_, velx_, vely_ = [], [], []
last_state, curr_steer, prev_steer = None, None, None
curr_ticks, prev_ticks = None, None
acumbx, acumby, acumbz = 0., 0., 0.
# to this must be included more bagfile's data.
dt_GPS = 0.58 # 580 ms
process_points = SigmaPoints(n=6, alpha=.80, beta=2., kappa=-2)
ukf = UKF( dim_x=6, dim_z=4, fx=F_, hx=H_, dt=None,
          points=process_points, x_mean_fn=state_mean, z_mean_fn=z_mean)

path_bagfile = '/media/diego/DataPart2/UC3M/2doCuatrimestre/Trabajo_LSI/data_icab/'
velofiles = glob.glob(path_bagfile + 'velo_test_1*.bag')
mapfiles  = glob.glob(path_bagfile + 'map_test*.bag')
velofiles.sort()
mapfiles.sort()
steer, vels = [], []
encoder, odometryData = [], []
odomx, odomy, odomvx, odomvy = 0, 0, 0, 0
angles, anglesacum , anglesGPS = [], [], []
posx, posy = [] , []
flagInit, imuInit=False, False
flagInitGPS = False
flagInitUKF = False
init_pos, vel_x, vel_y, init_orientation = None, None, None, None
for bagfile in velofiles[0:2]:
    # select the rosbagfile
    bag = rosbag.Bag((bagfile))
    print bagfile, 'is being read...'
    #for bagfile in mapfiles:
    # rostopics of interest depending on the bag file version
    if bagfile in velofiles:
        topic_POS2 = '/icab1/mavros/global_position/global' # type: nav_msgs/Odometry
        topic_POS = '/icab1/mavros/global_position/local' # type: nav_msgs/Odometry
        topic_ENC = '/icab1/movement_manager/status_info' # type: icab_msgs/StatusInfo
        topic_IMU = '/icab1/mavros/imu/data'              # type: sensor_msgs/Imu
    else:
        topic_POS2 ='/mavros/global_position/global' # type: nav_msgs/Odometry
        topic_POS = '/mavros/global_position/local' # type: nav_msgs/Odometry
        topic_ENC = '/movement_manager/status_info' # type: icab_msgs/StatusInfo
        topic_IMU = '/mavros/imu/data'              # type: sensor_msgs/Imu
    for topic_, msg_, t_ in bag.read_messages():
        if flagInit==False:
            lastTime = t_.to_sec()
            flagInit = True
            continue
        if topic_ == topic_POS:
            lastTimeGPS = t_.to_sec()
            init_pos = [msg_.pose.pose.position.x.real, msg_.pose.pose.position.y.real]
            vel_x = msg_.twist.twist.linear.x
            vel_y = msg_.twist.twist.linear.y
            __, __, init_orientation = quat2angle(msg_.pose.pose.orientation)

            hx, hy, hz = quat2angle(msg_.pose.pose.orientation)
            currTime = t_.to_sec()
            x = msg_.pose.pose.position.x.real
            y = msg_.pose.pose.position.y.real
            vel_x = msg_.twist.twist.linear.x
            vel_y = msg_.twist.twist.linear.y
            var_x = msg_.pose.covariance[0] # error en x
            var_y = msg_.pose.covariance[7] # error en y
            v_o = msg_.pose.covariance[35]
            R = np.diag((var_x, var_y, 2, 2))
            posx.append(x)
            posy.append(y)
            vels.append(( msg_.twist.twist.linear.x, msg_.twist.twist.linear.y, msg_.twist.twist.linear.z ))
            anglesGPS.append((hx, hy, hz))
            timeGps.append(t_.to_sec())
        #init-------------------------------------------------------#
        # UKF ------------------------------------------------------#
        #-----------------------------------------------------------#
        if init_pos is not None and vel_x is not None and vel_y is not None and init_orientation is not None and not flagInitUKF:
            #START THE UKF#
            ukf.x = np.array([init_pos[0], init_pos[1],  vel_x, vel_y, init_orientation, 0])
            ukf.P = np.diag([500, 500, 3, 3, 2, 2])
            ukf.Q = np.diag((2.5, 2.5, 2.0, 2.0, 1.5, 1.3))
            flagInitUKF = True
            ukfTime = t_.to_sec()
            
        if flagInitUKF and (round(t_.to_sec() - ukfTime) >= UKF_PERIOD):
            ukfTime = t_.to_sec()
            #-----PREDICT------#
            ukf.predict(dt =  UKF_PERIOD)
            dts.append(currTime - lastTimeGPS)
            odomx, odomy, odomvx, odomvy = odometry(ukf.x, delta_ticks, dt=UKF_PERIOD)
            #-----UPDATE------#
            ukf.update([x, y, vel_x, vel_y], R = R, hx_args=())
            if last_state is not None:
                #ukf.x[4] = ukf.x[4]+last_state[4]
                timeukf.append(t_.to_sec())
                odometryData.append((odomx, odomy, odomvx, odomvy))
                x_.append(ukf.x[0])
                y_.append(ukf.x[1])
                beta_.append( ukf.x[4])
                velx_.append(ukf.x[2])
                vely_.append(ukf.x[3])
                steer_.append(ukf.x[5])
                P_.append(ukf.P.diagonal())
            last_state = ukf.x
            lastTimeGPS = currTime
            odomx, odomy, odomvx, odomvy = 0, 0, 0, 0 # reset the internal encoder counters
        #end--------------------------------------------------------#
        # UKF ------------------------------------------------------#
        #-----------------------------------------------------------#
        if topic_ == topic_ENC :
            if prev_steer is not None:
                delta_steer = curr_steer - prev_steer
                delta_ticks = curr_ticks - prev_ticks
                #ukf.x[5] = delta_steer
                steer.append(msg_.dir.angle.real-prev_steer)
                timeEnc.append(t_.to_sec())
                encoder.append(msg_.trc.val_encoder.real)
            curr_steer = msg_.dir.angle.real
            curr_ticks = msg_.trc.val_encoder.real
            prev_steer = curr_steer
            prev_ticks = curr_ticks
            prev_time_enc = t_.to_sec()
        if topic_ == topic_IMU:
            if imuInit==False:
                prevTimeIMU = t_.to_sec()
                imuInit = True
            else:
                dt = t_.to_sec() - prevTimeIMU
                b_x, b_y, b_z = quat2angle( msg_.orientation )
                acumbx += (dt*msg_.angular_velocity.x)
                acumby += (dt*msg_.angular_velocity.y)
                acumbz += (dt*msg_.angular_velocity.z)
                #init_orientation += dt*b_z
                anglesacum.append((acumbx, acumby, acumbz))
                angles.append((+b_x, +b_y, +b_z))
                timeImu.append(t_.to_sec())
                prevTimeIMU = t_.to_sec()
#%%
P_ = np.array(P_)
posx = np.array(posx)
posy = np.array(posy)
steer = np.array(steer)
encoder = np.array(encoder)
vels = np.array(vels)
angles = np.array(angles)
anglesGPS = np.array(anglesGPS)
anglesacum = np.array(anglesacum)
beta_=np.array(beta_)
velx_=np.array(velx_)
odometryData = np.array(odometryData)
P_ = np.array(P_)
#%%
plt.figure(1)
plt.subplot(221)
plt.plot(posx, posy, 'b-',label='GPS', lw = 1) # data
plt.plot(+odometryData[:,0], +odometryData[:, 1], 'gp',label='Odometria', lw = 1) # Enc
plt.plot(x_, y_, 'r-', label='UKF',lw = 2) # filter
plt.legend(loc='upper left')
plt.axis('equal')
plt.title('Trayectoria iCab - Coord. UTM')

plt.subplot(222)
#plt.plot(timeImu, +angles[:,2], 'k-',label='ORIENTATION (IMU-quat)', lw = 1) # yaw data
plt.plot(timeImu, +anglesGPS[0,2]+anglesacum[:,2], 'g-',label='ORIENTATION (IMU-angularveloc)', lw = 1) # data
plt.plot(timeGps, +anglesGPS[:,2], 'b-',label='ORIENTATION (GPS)', lw = 2) # data
plt.plot(timeukf, +beta_[:], 'r-', label='UKF', lw = 2) # filter
plt.title('Orientacion vs Tiempo')
plt.legend(loc='upper right')

plt.subplot(223)
plt.plot(timeGps, posx, 'b-',label='GPS', lw = 1) # gps
plt.plot(timeukf, odometryData[:,0], 'g-', label='ENC', lw=1 ) # enc
plt.plot(timeukf, x_, 'r-', label='UKF',lw = 2) # filter
plt.title('Coord X-UTM vs Tiempo')
plt.legend(loc='upper right')

plt.subplot(224)
plt.plot(timeGps, posy, 'b-',label='GPS', lw = 1) # data
plt.plot(timeukf, odometryData[:,1], 'g-', label='ENC', lw=1 ) # enc
plt.plot(timeukf, y_, 'r-', label='UKF',lw = 2) # filter
plt.legend(loc='upper right')
plt.title('Coord Y-UTM vs Tiempo')


#%%

plt.figure(2)
plt.subplot(231)
plt.plot(posx, posy, 'b+',label='GPS', lw = 1) # data
plt.plot(odometryData[:,0], odometryData[:, 1], 'gp',label='GPS', lw = 1) # data
plt.plot(x_, y_, 'r-', label='UKF',lw = 2) # filter
plt.legend(loc='upper left')
plt.axis('equal')
plt.title('Trayectoria iCab - Coordenadas UTM')


plt.subplot(232)
#plt.plot(timeImu, +angles[:,2], 'k-',label='ORIENTATION (IMU-quat)', lw = 1) # yaw data
#plt.plot(timeImu, +init_orientation+anglesacum[:,2], 'g-',label='ORIENTATION (IMU-angularveloc)', lw = 1) # data
plt.plot(timeGps, +anglesGPS[:,2], 'b-',label='ORIENTATION (GPS)', lw = 2) # data
plt.plot(timeukf, +beta_[:], 'r-', label='UKF', lw = 2) # filter
plt.legend(loc='upper right')
plt.title('Orientacion acumulada (rad) vs Tiempo (seg)')

plt.subplot(233)
plt.plot(timeEnc, steer, 'b-',label='ENCODER', lw = 1) # data
plt.plot(timeukf, steer_, 'r-', label='UKF', lw = 2) # filter
plt.legend(loc='upper left')
plt.title('Angulo de direccion (rad) vs Tiempo (seg)')

plt.subplot(234)
plt.plot(timeGps, vels[:, 0], 'b-p', label='GPS-velocidad-x', lw = 1) # filter
plt.plot(timeukf, odometryData[:,2], 'g-p', label='ENC', lw=1 ) # enc
plt.plot(timeukf, velx_, 'r-o', label='UKF-velocidad-x', lw = 2) # filter
plt.legend(loc='upper left')
plt.title('Componente x de la Velocidad (m/seg) vs Tiempo (seg)')
#plt.axis('equal')

plt.subplot(235)
plt.plot(timeGps, vels[:, 1], 'b-p', label='GPS-velocidad-y', lw = 1) # filter
plt.plot(timeukf, odometryData[:,3], 'g-p', label='ENC', lw=1 ) # enc
plt.plot(timeukf, vely_, 'r-o', label='UKF-velocidad-y', lw = 2) # filter
plt.legend(loc='upper left')
plt.title('Componente y de la Velocidad (m/seg) vs Tiempo (seg)')
#plt.axis('equal')

import numpy as np
from constants import *
import math

def quat2angle(quat):
    roll   = math.atan2(2*(quat.w*quat.x+quat.y*quat.z),1-2*(quat.x**2+quat.y**2))
    pitch   = math.asin(2*(quat.w*quat.y+quat.z*quat.x))
    yaw   = math.atan2(2*(quat.w*quat.z+quat.x*quat.y),1-2*(quat.y**2+quat.z**2))
    return (roll, pitch, yaw)

def round_mat(P, n):
    for i in range(P.shape[0]):
        for j in range(P.shape[1]):
            P[i,j] = np.round(P[i,j],n)
    return P

def state_mean(sigmas, Wm):
    x = np.zeros(len(sigmas[0]))
    sum_sin_or, sum_cos_or, sum_sin_steer, sum_cos_steer = 0., 0., 0., 0.
    for i in range(len(sigmas)):
        s = sigmas[i]
        x[0] += s[0] * Wm[i]
        x[1] += s[1] * Wm[i]
        sum_sin_or += math.sin(s[2])*Wm[i]
        sum_cos_or += math.cos(s[2])*Wm[i]
        x[3] += s[3] * Wm[i]
        sum_sin_steer += math.sin(s[4])*Wm[i]
        sum_cos_steer += math.cos(s[4])*Wm[i]
    x[2] = math.atan2(sum_sin_or, sum_cos_or)
    x[4] = math.atan2(sum_sin_steer, sum_cos_steer)
    return x


def residual_h( a, b ):
    y = a - b
    if y.shape[0] == 2: # to the ENC sensor
        y[1] = normalize_steering(y[1])  # normalizing the steering angle
    if y.shape[0] == 3: # to the GPS sensor 
        y[2] = normalize_angle(y[2])  # normalizing the beta angle (orietnation)
    return y

def residual_x( a, b ):
    y = a - b
    y[0] = y[0]
    y[1] = y[1]
    y[2] = normalize_angle(y[2])
    y[3] = y[3]
    y[4] = normalize_steering(y[4])
    return y

def H_gps(x, dt, prev_z):
    z = np.array([x[0],x[1],x[2],x[3]]) # [x, y, vx, vy]
    return z

def H_steer_renc(x, dt, prev_z):
    dist = x[3]*dt
    if prev_z is None: prev_z= np.zeros(2)
    encoder = dist/wheel_circ*(ticks_per_rev - prev_z[1])/10
    z = np.array([normalize_steering(x[4]), encoder])
    return z

def H_imu(x, dt, prev_z):
    return (x[2])/dt

def normalize_angle3(x):
    if x > 0:
        return x % (np.pi / 2)
    else:
        return (np.pi/2)-(x % (np.pi/2))
    return x

def normalize_angle2(x):
    if x > 0:
        return x % (2*np.pi)
    else:
        return (2*np.pi - (x % (2*np.pi)))
    return x
    
def normalize_angle(x):
#    if x >= 0:
#        return x % (2*np.pi)
#    else:
#        return (2*np.pi - (x % (2*np.pi)))
    return x

def normalize_steering(x):
    # must be in [-10, 10] deg for stage
#    b = v_steer*0.01
#    if x <= -b: x = (-b)
#    if x > ( b): x = ( b)
    return x

def F__( x, dt, fx_args=() ):
    if not isinstance( fx_args, tuple ):
        fx_args = (fx_args, )
    # labelling the state vector components
    beta = x[2]
    vel  = x[3]
    steering_angle = x[4]
    # distance of displacement
    dist = vel*dt
    # turning
    if  abs(steering_angle) > 0.0001:
        dx_ = np.power(vel,2)*np.power(dt,2)*np.tan(steering_angle)/(2*wheelbase)
        dy_ = np.sqrt(dist**2-dx_**2)
        Rot = np.array([[np.cos( -beta ), -np.sin( -beta )],[np.sin( -beta ),  np.cos( -beta )]])
        result = np.cross(Rot, np.array([dx_, dy_]).T)
        dx_=result[0]
        dy_=result[1]
        r = wheelbase/np.tan(abs(steering_angle))
        dbeta = (np.math.atan2(dist/2, r))*dt
        #print  dbeta
    # going straight forward
    else:
        dbeta = 0
        dx_ = dist*np.sin(beta)
        dy_ = dist*np.cos(beta)
    x += np.array([dx_, dy_, dbeta, 0, 0])
    x[2] = normalize_angle(x[2])
    x[4] = normalize_steering(x[4])
    return x

def F_(x, dt, fx_args=()): # state vector : [x, y, vx, vy, heading, steer] 6x1
    if not isinstance( fx_args, tuple):
        fx_args = (fx_args, )
    #hdg = x[4]
    steering_angle = x[4] #x[5]
    #vx = x[2]
    #vy = x[3]
    #distx = vx*dt
    #disty = vy*dt
    #dist=np.sqrt(distx**2+disty**2)
    vel = x[3]
    dist = vel*dt
    hdg = x[2]
    if abs(np.degrees(steering_angle)) > 0.5:
        beta = (dist/wheelbase)*np.tan(steering_angle)
        # the icc's radius r
        r = wheelbase / np.tan(steering_angle)
        sinh, sinhb = np.sin(hdg), np.sin(hdg + beta)   
        cosh, coshb = np.cos(hdg), np.cos(hdg + beta)
        x += np.array([-r*sinh + r*sinhb, r*cosh - r*coshb, beta, 0, 0])
        return x
    else:
        return x + np.array([dist*np.cos(hdg), dist*np.sin(hdg), 0, 0, 0])

def wheel_odom( pos, beta, dt, enc ):
    steering_angle = enc.value[0]
    # distance of displacement
    dist = enc.value[1]*wheel_circ/ticks_per_rev
    vel = dist/dt
    # turning
    if  abs(steering_angle) > 0.01:
        dx_ = np.power(vel,2)*np.power(dt,2)*np.tan(steering_angle)/(2*wheelbase)
        dy_ = np.sqrt(np.power(dist,2)-np.power(dx_,2))
        Rot = np.array([[np.cos( -beta ), -np.sin( -beta )],
                        [np.sin( -beta ),  np.cos( -beta )]])
        result = np.cross(Rot, np.array([dx_, dy_]).T)
        dx_=result[0]
        dy_=result[1]
        r = wheelbase/np.tan(abs(steering_angle))
        dbeta = (np.math.atan2(dist/2, r))
        #print  dbeta
    # going straight forward
    else:
        dbeta = 0
        dx_ = dist*np.sin(beta)
        dy_ = dist*np.cos(beta)
    pos[0] += dx_
    pos[1] += dy_
    beta += dbeta
    beta = normalize_angle(beta)
    return (pos, beta)


def wheel_odometry(pos, orien, steer, enc_sensor):
    d = enc_sensor.value[1]*wheel_circ/ticks_per_rev
    if abs(enc_sensor.value[0]-enc_sensor.prev_value[0]) > 0.00001:
        r = wheelbase/np.tan(enc_sensor.value[0]-enc_sensor.prev_value[0])
        dbeta = (np.math.atan2( d/2, r ))
    else:
        dbeta = 0
    dx_ = d*np.sin(normalize_angle(orien))
    dy_ = d*np.cos(normalize_angle(orien))
    orien += dbeta
    return (pos[0]+dx_, pos[1]+dy_, normalize_angle(orien))
